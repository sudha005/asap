<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Productweights extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
    }
     public function index() {
        self::viewProductweights();
      }
    public function addProductweights(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $dataBefore =[];
        $product_weight =$this->input->post('product_weight');
        $resultCategory = $this->Adminmodel->getMasterCategory('master_category');
        $dataBefore['resultCnt'] = $resultCategory; 
        if(!empty($product_weight)){
            $id = $this->input->post('id');
            $master_category_id = $this->input->post('master_category_id');
            $category_id = $this->input->post('category_id');
            $subcategory_id = $this->input->post('subcategory_id');
            if($product_weight!=''){
                $check_data = array(
                "master_category_id" => $master_category_id,
                "category_id" => $category_id,
                "subcategory_id" => $subcategory_id,
                "product_weight" => $product_weight,
                "id !=" =>$id   
                );
                $tablename = "prodcut_weights";
                $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
                if($checkData > 0){
                    $this->session->set_flashdata('msg','<div class="alert alert-danger">Product Weights already exist</div>') ;
                }else{
                    $admin = $this->session->userdata('userCode');
                    $added_by = $admin!='' ? $admin:'admin' ; 
                    $dateCurrent= date("Y-m-d H:i:s");
                    $master_category_id = $this->input->post('master_category_id') =="" ? "":$this->input->post('master_category_id');
                    $category_id = $this->input->post('category_id') =="" ? "":$this->input->post('category_id');
                    $subcategory_id = $this->input->post('subcategory_id') =="" ? "":$this->input->post('subcategory_id');
                    $dataCity = array(
                        'master_category_id'=> $master_category_id,
                        'category_id'=> $category_id,
                        'subcategory_id'=> $subcategory_id,
                        'product_weight'=> $product_weight,
                        'created_by'     => $admin ,
                        'created_at'     => $dateCurrent,
                        'updated_at'     => $dateCurrent,
                        'updated_by'     => $added_by
                    );
                    $tableCity="prodcut_weights";
                    $result = $this->Adminmodel->insertRecordQueryList($tableCity,$dataCity);
                    if($result) {
                        $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Product Weights Added Successfully</div>') ;
                    } else{
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Product not Added</div>') ;
                    }
                }
                redirect('viewProductweights');
            } 
        }
        else{
                /*$this->session->set_flashdata('msg','<div class="alert alert-danger">fail</div>') ;*/
                $this->load->view('admin/add_productweights',$dataBefore);   
        }
    }
        public function viewProductweights(){
            if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
            {
              redirect('admin');
            }
                   
            $table ="prodcut_weights";
            $search = ($this->input->get("search"))? $this->input->get("search") : "null";
           $config = array();
           $config['reuse_query_string'] = true;
           $config["base_url"] = base_url() . "Productweights/viewProductweights";
           $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
           $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'product_weight');//search
           $config["per_page"] = PERPAGE_LIMIT;
           $config["uri_segment"] = 3;
           $config['full_tag_open'] = "<ul class='pagination'>";
           $config['full_tag_close'] = '</ul>';
           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';
           $config['cur_tag_open'] = '<li class="active"><a href="#">';
           $config['cur_tag_close'] = '</a></li>';
           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';
           $config['first_tag_open'] = '<li>';
           $config['first_tag_close'] = '</li>';
           $config['last_tag_open'] = '<li>';
           $config['last_tag_close'] = '</li>';
           $config['prev_link'] = '<i class="mdi mdi-skip-backward"></i>';
           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';
           $config['next_link'] = '<i class="mdi mdi-skip-forward"></i>';
           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';
           $this->pagination->initialize($config);
           $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
           $data["links"] = $this->pagination->create_links();
           $limit =$config["per_page"];
           $start=$page;
           $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'product_weight');
                    if($result){
                        foreach ($result as $key => $field) {
                            $result[$key]['mastercategory'] = $this->Adminmodel->getSingleColumnName($field['master_category_id'],'id','master_category_name','master_category') ;
                            $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field['category_id'],'id','category_name','category') ;
                            $result[$key]['subcategory'] = $this->Adminmodel->getSingleColumnName($field['subcategory_id'],'id','subcategory_name','subcategory');
                        } 
                        $data['result'] = $result;
                    } else {
                        $result[] = [] ;
                        $data['result'] = $result ;
                    }
                    $data['searchVal'] = $search !='null'?$search:"";
                    $this->load->view('admin/view_productweights',$data);
                }
  public function editProductweights(){
      if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
      {
        redirect('admin');
      }
      $id = $this->uri->segment('3');
      $resultCategory = $this->Adminmodel->getMasterCategory('master_category');
        $data['resultCnt'] = $resultCategory; 
      if($id==''){
          redirect('adminLogin');
      }
      $tablename = "prodcut_weights";
      $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
      if($result) {
          foreach ($result as $key => $field) {
              $result[$key]['mastercategory'] = $this->Adminmodel->getSingleColumnName($field['master_category_id'],'id','master_category_name','master_category') ;
              $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field['category_id'],'id','category_name','category') ;
              $result[$key]['subcategory'] = $this->Adminmodel->getSingleColumnName($field['subcategory_id'],'id','subcategory_name','subcategory');
          }
          $data['result'] = $result[0];
          $this->load->view('admin/edit_productweights',$data);
      } else {
          $url='viewProductweights';
          redirect($url);
      }
    }
    public function updateProductweights(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
        $product_weight = $this->input->post('product_weight');       
        if($product_weight!=''){            
            $check_data = array(
                "product_weight" => $product_weight,
                "id !=" =>$id   
            );
            $tablename = "prodcut_weights";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Productweights already exist</div>') ;
            }else{
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;           
                $date     = date("Y-m-d H:i:s");                
                $master_category_id = $this->input->post('master_category_id')=="" ? "":$this->input->post('master_category_id');
                $category_id = $this->input->post('category_id')=="" ? "":$this->input->post('category_id');
                 $subcategory_id = $this->input->post('subcategory_id') =="" ? "":$this->input->post('subcategory_id');
                $data = array(
                    'master_category_id'=> $master_category_id,
                    'category_id'=> $category_id,
                    'subcategory_id'=> $subcategory_id,
                    'product_weight'=> $product_weight,
                    'created_by'     => $added_by ,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                $table="prodcut_weights";
                $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                if($result){
                    $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">Product weights Updated.</div>');
                }else{
                    $url='Productweights/editProductweights/'.$id;
                    redirect($url);
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps! Some error,Product weights not updated.</div>') ;
                }   
            } 
            redirect('viewProductweights');
        }else {   
            $url='Productweights/editProductweights/'.$id;
            redirect($url); 
        }
    }
            
    
        function productweightsEnable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'isactive' =>'0'
            );
            $table="prodcut_weights";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='Productweights/viewProductweights';
            redirect($url);
        }      
        function productweightsDisable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'isactive' =>'1'
            );
            $table="prodcut_weights";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='Productweights/viewProductweights';
            redirect($url);
        }
        function deleteProductweights($id) {
        $id=$id;
        $result = $this->Adminmodel->delRow($id,'prodcut_weights');
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }
}
?>
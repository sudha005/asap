<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productprice extends CI_Controller {
    function __construct() {
        parent::__construct();
         $this->load->helper("encryptionpwd");
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->library('form_validation');
        $this->load->library('upload'); 
    }
    public function index() {
        self::viewProductprice();
    } 

    public function viewProductprice(){
        if(!is_branchlogged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('product_weights_prices');
        }
        $table ="product_weights_prices";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Productprice/viewProductprice";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'product_id');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="mdi mdi-skip-backward"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="mdi mdi-skip-forward"></i>'; 
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $branch_code = $this->session->userdata('branchCode');
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,'branch_code',$branch_code,$search,'product_id');
       if($result) {
            foreach ($result as $key => $field) {
                $result[$key]['product'] = $this->Adminmodel->getSingleColumnName($field['product_id'],'id','product_name','vendor_products') ;
                $result[$key]['proweight'] = $this->Adminmodel->getSingleColumnName($field['product_weight_id'],'id','product_weight','prodcut_weights') ;
            }
        }
        if($result){
            $data['result'] = $result ;
        } else {
            $result[] = [] ;
            $data['result'] = $result ;
        }
        $data['searchVal'] = $search !='null'?$search:"";  
        $this->load->view('branch/view_productprice',$data);
    }  

    public function addProductprice(){
        if(!is_branchlogged_in())  // if you add in constructor no need write each function in above controller.
        {
        redirect('Masterbranch');
        }
        $resultProduct = $this->Adminmodel->getMasterCategory('vendor_products');
        $dataBefore['resultProduct'] = $resultProduct;
        $product_price = $this->input->post('product_price');   
        $branch = $this->session->userdata('branchCode');    
        if($product_price!=''){    
            $check_data = array(
                "branch_code" => $branch,
                "product_id" => $this->input->post('product_id'),
                "product_weight_id" => $this->input->post('product_weight_id')
            );
            $tablename = "product_weights_prices";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Product Price already exist</div>') ;
                $this->load->view('branch/add_Productprice',$dataBefore);
            }else{
                $branch_vendorCode = $this->session->userdata('branch_vendorCode');
                $added_by = $branch!='' ? $branch:'branch' ;           
                $date     = date("Y-m-d H:i:s");
                $product_id = $this->input->post('product_id')=="" ? "":$this->input->post('product_id');
                $product_sub_cat_id = $this->Adminmodel->getSingleColumnName($product_id,'id','sub_category_id','vendor_products');
                $product_weight_id = $this->input->post('product_weight_id')=="" ? "":$this->input->post('product_weight_id');    
                $data = array(
                    'vendor_code'=> $branch_vendorCode ,
                    'branch_code'=> $added_by ,
                    'product_id'=> $product_id ,  
                    'product_sub_cat_id' =>  $product_sub_cat_id,
                    'product_weight_id' =>  $product_weight_id,
                    'product_price'   =>  $product_price,
                    'created_by'     => $added_by ,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                $table="product_weights_prices";
                $result = $this->Adminmodel->insertRecordQueryList($table,$data);
                if($result){
                    $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Product Price Inserted</div>');
                }
                else{
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Product Price not inserted</div>') ;
                }
                redirect('viewProductprice');
            }
        }else {
            $this->load->view('branch/add_Productprice',$dataBefore);    
        }       
    }
    
    public function editProductprice(){
        if(!is_branchlogged_in())  // if you add in constructor no need write each function in above controller.
        {
        redirect('Masterbranch');
        }
        $id = $this->uri->segment('3');
        $resultProduct = $this->Adminmodel->getMasterCategory('vendor_products');
        $data['resultProduct'] = $resultProduct;
        if($id==''){
            redirect('branchLogin');
        }
        $tablename = "product_weights_prices";
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
        if($result) {
            foreach ($result as $key => $field) {
                $result[$key]['product'] = $this->Adminmodel->getSingleColumnName($field['product_id'],'id','product_name','vendor_products') ;
                $result[$key]['proweight'] = $this->Adminmodel->getSingleColumnName($field['product_weight_id'],'id','product_weight','prodcut_weights') ;
            }
            $data['result'] = $result[0] ;
            $this->load->view('branch/edit_Productprice',$data);
        } else {
            $url='viewProductprice';
            redirect($url);
        }
    }
    public function updateProductprice(){
        if(!is_branchlogged_in())  // if you add in constructor no need write each function in above controller.
        {
        redirect('Masterbranch');
        }
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('branchLogin');
        }
        $product_price = $this->input->post('product_price');
        $branch = $this->session->userdata('branchCode');    
        if($product_price!=''){            
            $check_data = array(
                "branch_code" => $branch,
                "product_id" => $this->input->post('product_id'),
                "product_weight_id" => $this->input->post('product_weight_id'),
                "id !=" => $this->input->post('id')
            );
            $tablename = "product_weights_prices";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Product Price already exist</div>') ;
                $url='Productprice/editProductprice/'.$id;
                redirect($url);
            }else{
                $branch_vendorCode = $this->session->userdata('branch_vendorCode');
                $added_by = $branch!='' ? $branch:'branch' ;           
                $date     = date("Y-m-d H:i:s");
                $product_id = $this->input->post('product_id')=="" ? "":$this->input->post('product_id');
                $product_sub_cat_id = $this->Adminmodel->getSingleColumnName($product_id,'id','sub_category_id','vendor_products');
                $product_weight_id = $this->input->post('product_weight_id')=="" ? "":$this->input->post('product_weight_id');    
                $data = array(
                    'vendor_code'=> $branch_vendorCode ,
                    'branch_code'=> $added_by ,
                    'product_id'=> $product_id ,  
                    'product_sub_cat_id' =>  $product_sub_cat_id,
                    'product_weight_id' =>  $product_weight_id,
                    'product_price'   =>  $product_price,
                    'created_by'     => $added_by ,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                $table="product_weights_prices";
                $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                if($result){
                    $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">Product Price Updated.</div>');
                }else{
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps! Some error, Product Price not updated.</div>') ;
                }   
            } 
            redirect('viewProductprice');
        }else {   
            $url='Productprice/editProductprice/'.$id;
            redirect($url); 
        }
    }
    function ProductpriceEnable($id) {
        $id=$id;
        $dataproweight =array(
            'isactive' =>'0'
        );
        $table="product_weights_prices";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataproweight,'id',$id);
        $url='viewProductprice';
            redirect($url);
    }      
    function ProductpriceDisable($id) {
        $id=$id;
        $dataproweight =array(
            'isactive' =>'1'
        );
        $table="product_weights_prices";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataproweight,'id',$id);
        $url='viewProductprice';
        redirect($url);
    }
    public function proweight(){
        $id =$this->input->post('id');
        $subcategoryId = $this->Adminmodel->getSingleColumnName($id,'id','sub_category_id','vendor_products');
        $result = $this->Adminmodel->getAjaxdata('subcategory_id',$subcategoryId,'prodcut_weights');
        $data['resultproweight'] =$result;
        $this->load->view('branch/proweightAjax',$data);
    }
    function deleteProductprice($id) {
        $id=$id;
        $result = $this->Adminmodel->delRow($id,'product_weights_prices');
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }
}
?>
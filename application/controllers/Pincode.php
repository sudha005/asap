<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pincode extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
    }
    public function index() {
         self::viewpincode();
      } 
     public function addpincode(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
       
        $dataBefore =[];
        $pincode =$this->input->post('pincode');
        $this->load->library('upload');       
        $resultCountry = $this->Adminmodel->getMasterCategory('countries');
        $dataBefore['resultCnt'] = $resultCountry;
        $country_id = $this->input->post('country_id');
        $state_id = $this->input->post('state_id');
        $district_id = $this->input->post('district_id');
        $city_id = $this->input->post('city_id');
        if(!empty($pincode)){
            $check_data = array(
                "country_id" => $country_id,
                "state_id" => $state_id,
                "district_id"=>$district_id, 
                "city_id" => $city_id,
                "pincode" => $pincode
            );
            $tablename = "pincodes";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger">Pincode already exist</div>') ;
                $this->load->view('admin/add_pincode',$dataBefore);
            }else{
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ; 
                $dateCurrent= date("Y-m-d H:i:s");
                $country_id = $this->input->post('country_id') =="" ? "":$this->input->post('country_id');
                $state_id = $this->input->post('state_id') =="" ? "":$this->input->post('state_id');
                $city_id = $this->input->post('city_id') =="" ? "":$this->input->post('city_id');

                $district_id = $this->input->post('district_id') =="" ? "":$this->input->post('district_id');
                $pincode = $this->input->post('pincode') =="" ? "":$this->input->post('pincode');
                $dataCity = array(
                    'country_id'=> $country_id,
                    'state_id'=> $state_id,
                    'district_id'=>$district_id,
                    'city_id'=> $city_id,
                    'pincode'=>  $pincode,
                    'created_by'     => $admin ,
                    'created_at'     => $dateCurrent,
                    'updated_at'     => $dateCurrent,
                    'updated_by'     => $added_by,
                );
                $tablePincode="pincodes";
                $result = $this->Adminmodel->insertRecordQueryList($tablePincode,$dataCity);
                if($result){
                $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Pincode Added Successfully</div>') ;
                }else{
                   $this->session->set_flashdata('msg','<div class="alert alert-danger">opp! not inserted</div>') ;
                }           
                redirect('viewpincode');
            }
        } else{
            /*$this->session->set_flashdata('msg','<div class="alert alert-danger">fail</div>') ;*/
            $this->load->view('admin/add_pincode',$dataBefore);   
        }
    }
    public function viewpincode(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }      
            $table ="pincodes";
            $search = ($this->input->get("search"))? $this->input->get("search") : "null";
           $config = array();
           $config['reuse_query_string'] = true;
           $config["base_url"] = base_url() . "pincode/viewpincode";
           $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
           $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'pincode');//search
           $config["per_page"] = PERPAGE_LIMIT;
           $config["uri_segment"] = 3;
           $config['full_tag_open'] = "<ul class='pagination'>";
           $config['full_tag_close'] = '</ul>';
           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';
           $config['cur_tag_open'] = '<li class="active"><a href="#">';
           $config['cur_tag_close'] = '</a></li>';
           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';
           $config['first_tag_open'] = '<li>';
           $config['first_tag_close'] = '</li>';
           $config['last_tag_open'] = '<li>';
           $config['last_tag_close'] = '</li>';
           $config['prev_link'] = '<i class="mdi mdi-skip-backward"></i>';
           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';
           $config['next_link'] = '<i class="mdi mdi-skip-forward"></i>';         
           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';
           $this->pagination->initialize($config);
           $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
           $data["links"] = $this->pagination->create_links();
           $limit =$config["per_page"];
           $start=$page;
           $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'pincode');
                    if($result){
                        foreach ($result as $key => $field) {
                            $result[$key]['country'] = $this->Adminmodel->getSingleColumnName($field['country_id'],'id','country_name','countries');
                            $result[$key]['state'] = $this->Adminmodel->getSingleColumnName($field['state_id'],'id','state_name','states');
                            $result[$key]['district'] = $this->Adminmodel->getSingleColumnName($field['district_id'],'id','district_name','districts');
                            $result[$key]['city'] = $this->Adminmodel->getSingleColumnName($field['city_id'],'id','city_name','cities');
                        } 
                        $data['result'] = $result;
                    } else {
                        $result[] = [] ;
                        $data['result'] = $result ;
                    }
                    $data['searchVal'] = $search !='null'?$search:"";
                    $this->load->view('admin/view_pincode',$data);
                
                }
            
    // Edit pincode
    public function editpincode(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
       
        $id = $this->uri->segment('3');
        if($id==''){
            redirect('adminLogin');
        }
        $tablename = "pincodes";
        $tablename2 = "countries";
        $start =0;
        $limit =100;
        $data['country']= $this->Adminmodel->get_current_page_records($tablename2,$limit,$start,$column=null,$value=null);
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
            foreach ($result as $key => $field) {
                $result[$key]['country'] = $this->Adminmodel->getSingleColumnName($field['country_id'],'id','country_name','countries');
                $result[$key]['state'] = $this->Adminmodel->getSingleColumnName($field['state_id'],'id','state_name','states');
                $result[$key]['district'] = $this->Adminmodel->getSingleColumnName($field['district_id'],'id','district_name','districts');
                $result[$key]['city'] = $this->Adminmodel->getSingleColumnName($field['city_id'],'id','city_name','cities');
            }
        $data['result'] = $result[0];
        if($result) {
         $this->load->view('admin/edit_pincode',$data);
        }else {
          $url='viewpincode';
          redirect($url);
             }  
        }
        public function updatepincode(){
            if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
            {
              redirect('admin');
            }
           
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
         $country_id = $this->input->post('country_id');
         $state_id = $this->input->post('state_id');
         $pincode = $this->input->post('pincode');
         $district_id = $this->input->post('district_id');
         $city_id = $this->input->post('city_id');
         if($pincode!=''){
             $check_data = array(
             "country_id" => $country_id,
             "state_id" => $state_id,
             "district_id"=>$district_id, 
             "city_id" => $city_id,
             "pincode" => $pincode,
             "id !=" =>$id   
             );
             $tablename = "pincodes";
             $checkData = $this->Adminmodel->existData($check_data,$tablename) ;

             if($checkData > 0){
                 $this->session->set_flashdata('msg','<div class="alert alert-danger">Pincode already exist</div>') ;
             }else{
                 $admin = $this->session->userdata('userCode');
                 $added_by = $admin!='' ? $admin:'admin' ;          
                 $date     = date("Y-m-d H:i:s");
                 $id =$this->input->post('id');
                 $dataSubcat = array(
                     "country_id" => $country_id,
                     "state_id" => $state_id,
                     "district_id"=>$district_id,
                     "city_id" => $city_id,
                     "pincode" => $pincode,
                     "updated_at" => $date,
                     "updated_by" => $added_by,
                 );
                 $table="pincodes";
                 $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
                 if($result){
                         $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Pincode Updated</div>');
                        
                 }
                 else{
                         $this->session->set_flashdata('msg','<div class="alert alert-danger">Opps Some error</div>') ;
                 } 
                 redirect('viewpincode');
             } 
             $url='pincode/editpincode/'.$id;
             redirect($url);
         }
         else
         {   
             $url='pincode/editpincode/'.$id;
             redirect($url);    
         }

    }
        function pincodeEnable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'isactive' =>'0'
            );
            $table="pincodes";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='pincode/viewpincode';
            redirect($url);
        }      
        function pincodeDisable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'isactive' =>'1'
            );
            $table="pincodes";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='pincode/viewpincode';
            redirect($url);
        }
        public function pincode(){
            $id =$this->input->post('id');
            $result = $this->Adminmodel->getAjaxdata('city_id',$id,'pincodes');
            $data['resultPincode'] =$result;
            $this->load->view('admin/pincodeAjax',$data);
       }
       function deletepincode($id) {
        $id=$id;
        $result = $this->Adminmodel->delRow($id,'pincodes');
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }
}
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends CI_Controller {
    function __construct() {
        parent::__construct();
         $this->load->helper("encryptionpwd");
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->library('form_validation');
        $this->load->library('upload'); 
    }
    public function index() {
        self::viewVendor();
    } 

    public function viewVendor(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $table ="vendor";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Vendor/viewVendor";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'name');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="mdi mdi-skip-backward"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="mdi mdi-skip-forward"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'name');
        if($result){
            $data['result'] = $result ;
        } else {
            $result[] = [] ;
            $data['result'] = $result ;
        }
        $data['searchVal'] = $search !='null'?$search:"";  
        $this->load->view('admin/view_Vendor',$data);
    }  

    public function addVendor(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $mobile = $this->input->post('mobile');
        $resultCategory = $this->Adminmodel->getMasterCategory('category');
        $resultCusine = $this->Adminmodel->getMasterCategory('cusine_types');
        $dataBefore['resultCat'] = $resultCategory;
        $dataBefore['resultCusine'] = $resultCusine;       
        if($mobile!=''){            
            $check_data = array(
            "mobile" => $this->input->post('mobile')
            );
            $tablename = "vendor";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Vendor already exist</div>') ;
                $this->load->view('admin/add_vendor',$dataBefore);
            }else{
                $min='1452';
                $max='8569';
                $vendor_code =rand($min,$max);
                if ($_FILES['vendor_pan_image']['size'] > 0) {
                    $config_media['upload_path'] = './uploads/vendor_pan_image';
                    $config_media['allowed_types'] = 'jpeg|gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';   
                    $config_media['max_size']   = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('vendor_pan_image'))
                    {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else
                    {
                        $data[] = array('upload_image' => $this->upload->data());
                    }       
                    $vendor_pan_image    = $data[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in Vendor pan image uploads</div>') ;
                        redirect('addVendor');
                    }        
                } else {
                    $vendor_pan_image    = "";
                }
                if ($_FILES['vendor_adhar_image']['size'] > 0) {
                   
                    $config_media1['upload_path'] = './uploads/vendor_adhar_image';
                    $config_media1['allowed_types'] = 'jpeg|gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';   
                    $config_media1['max_size']   = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media1);
                    $error = [];
                    if ( ! $this->upload->do_upload('vendor_adhar_image'))
                    {
                       
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else
                    {  
                        $data1[] = array('upload_image' => $this->upload->data());
                    }       
                    $vendor_adhar_image    = $data1[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in Vendor adhar image uploads</div>') ;
                        redirect('addVendor');
                    }        
                } else {
                    $vendor_adhar_image    = "";
                }
               
                if ($_FILES['business_pan_mage']['size'] > 0) {
                    $config_media2['upload_path'] = './uploads/business_pan_mage';
                    $config_media2['allowed_types'] = 'jpeg|gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';   
                    $config_media2['max_size']   = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media2);
                    $error = [];
                    if ( ! $this->upload->do_upload('business_pan_mage'))
                    {
                        
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else
                    {
                        $data2[] = array('upload_image' => $this->upload->data());
                    }       
                    $business_pan_mage    = $data2[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in business pan image uploads</div>') ;
                       // redirect('addVendor');
                    }        
                } else {
                    $business_pan_mage    = "";
                }
                
                if ($_FILES['registration_certificate_image']['size'] > 0) {
                    $config_media3['upload_path'] = './uploads/registration_certificate_image';
                    $config_media3['allowed_types'] = 'jpeg|gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';   
                    $config_media3['max_size']   = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media3);
                    $error = [];
                    if ( ! $this->upload->do_upload('registration_certificate_image'))
                    {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else
                    {
                        $data3[] = array('upload_image' => $this->upload->data());
                    }       
                    $registration_certificate_image    = $data3[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in business adhar image uploads</div>') ;
                        redirect('addVendor');
                    }        
                } else {
                    $registration_certificate_image    = "";
                }
                if ($_FILES['web_logo']['size'] > 0) {
                    $config_media4['upload_path'] = './uploads/web_logo';
                    $config_media4['allowed_types'] = 'jpeg|gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';   
                    $config_media4['max_size']   = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media4);
                    $error = [];
                    if ( ! $this->upload->do_upload('web_logo'))
                    {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else
                    {
                        $data4[] = array('upload_image' => $this->upload->data());
                    }       
                    $web_logo    = $data4[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in Vendor weblogo uploads</div>') ;
                        redirect('addVendor');
                    }        
                } else {
                    $web_logo    = "";
                }
                if ($_FILES['app_logo']['size'] > 0) {
                    $config_media5['upload_path'] = './uploads/app_logo';
                    $config_media5['allowed_types'] = 'jpeg|gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';   
                    $config_media5['max_size']   = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media5);
                    $error = [];
                    if ( ! $this->upload->do_upload('app_logo'))
                    {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else
                    {
                        $data5[] = array('upload_image' => $this->upload->data());
                    }       
                    $app_logo    = $data5[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in Vendor app logo uploads</div>') ;
                        redirect('addVendor');
                    }        
                } else {
                    $app_logo    = "";
                }
                if ($_FILES['web_restaurant_image']['size'] > 0) {
                    $config_media6['upload_path'] = './uploads/web_restaurant_image';
                    $config_media6['allowed_types'] = 'jpeg|gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';   
                    $config_media6['max_size']   = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media6);
                    $error = [];
                    if ( ! $this->upload->do_upload('web_restaurant_image'))
                    {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else
                    {
                        $data6[] = array('upload_image' => $this->upload->data());
                    }       
                    $web_restaurant_image    = $data6[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in Vendor web restaurant image uploads</div>') ;
                        redirect('addVendor');
                    }        
                } else {
                    $web_restaurant_image    = "";
                }
                if ($_FILES['app_restaurant_image']['size'] > 0) {
                    $config_media7['upload_path'] = './uploads/app_restaurant_image';
                    $config_media7['allowed_types'] = 'jpeg|gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';   
                    $config_media7['max_size']   = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media7);
                    $error = [];
                    if ( ! $this->upload->do_upload('app_restaurant_image'))
                    {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else
                    {
                        $data7[] = array('upload_image' => $this->upload->data());
                    }       
                    $app_restaurant_image    = $data7[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in Vendor app restaurant image uploads</div>') ;
                        redirect('addVendor');
                    }        
                } else {
                    $app_restaurant_image    = "";
                }
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;           
                $date     = date("Y-m-d H:i:s");
                $email = $this->input->post('email')=="" ? "":$this->input->post('email');
                $password = $this->input->post('password')=="" ? "":$this->input->post('password');
                $name = $this->input->post('name')=="" ? "":$this->input->post('name');
                $vendor_pan_number = $this->input->post('vendor_pan_number')=="" ? "":$this->input->post('vendor_pan_number');
                $vendor_adhar_number = $this->input->post('vendor_adhar_number')=="" ? "":$this->input->post('vendor_adhar_number');
                $address = $this->input->post('address')=="" ? "":$this->input->post('address');
                $business_name = $this->input->post('business_name')=="" ? "":$this->input->post('business_name');
                $restaurant_name = $this->input->post('restaurant_name')=="" ? "":$this->input->post('restaurant_name');
                $restaurant_type = $this->input->post('restaurant_type')=="" ? "":$this->input->post('restaurant_type');
                $business_pan_number = $this->input->post('business_pan_number')=="" ? "":$this->input->post('business_pan_number');
                $registration_certificate_no = $this->input->post('registration_certificate_no')=="" ? "":$this->input->post('registration_certificate_no');
                $established_year = $this->input->post('established_year')=="" ? "":$this->input->post('established_year');
                $make_it_popular = $this->input->post('make_it_popular')=="" ? "":$this->input->post('make_it_popular');
                $tag_detail = $this->input->post('tag_detail')=="" ? "":$this->input->post('tag_detail');
                //FOR Categories
                $i=0; 
                $catCount = count($this->input->post('category_id'));
                for($i = 0; $i < $catCount; $i++){
                    $data = array(
                        'vendor_code'=> $vendor_code ,
                        'category_id' => $this->input->post('category_id')[$i],
                    );
                    $result = $this->Adminmodel->insertRecordQueryList('vendor_service_categories',$data);
                }
                //FOR Cusine Types
                $i=0; 
                $cusineCount = count($this->input->post('cusine_type_id'));
                for($i = 0; $i < $cusineCount; $i++){
                    $data = array(
                        'vendor_code'=> $vendor_code ,
                        'cusine_type_id' => $this->input->post('cusine_type_id')[$i],
                    );
                    $result = $this->Adminmodel->insertRecordQueryList('vendor_service_cusinetypes',$data);
                }
                //FOR Vendor Login
                $vendorData = array(
                    'vendor_code'=> $vendor_code ,
                    'username' => $name,
                    'password' => $password,
                    'useremail' => $email,
                );
                $result = $this->Adminmodel->insertRecordQueryList('vendor_login',$vendorData);
                $data = array(
                    'name'=> $name ,                    
                    'email'=>  $email,
                    'mobile'=>  $mobile,
                    'password'=>  $password,
                    'vendor_pan_number'=>  $vendor_pan_number,
                    'vendor_adhar_number'=>  $vendor_adhar_number,
                    'address'=>  $address,
                    'business_name'=>  $business_name,
                    'restaurant_name'=>  $restaurant_name,
                    'restaurant_type'=>  $restaurant_type,
                    'business_pan_number'=>  $business_pan_number,
                    'registration_certificate_no'=>  $registration_certificate_no,
                    'established_year'=>  $established_year,
                    'vendor_code'=>  $vendor_code,
                    'vendor_pan_image'   =>  $vendor_pan_image,
                    'vendor_adhar_image'   =>  $vendor_adhar_image,
                    'business_pan_mage'   =>  $business_pan_mage,
                    'registration_certificate_image'   =>  $registration_certificate_image,
                    'web_logo'   =>  $web_logo,
                    'app_logo'   =>  $app_logo,
                    'web_restaurant_image'   =>  $web_restaurant_image,
                    'app_restaurant_image'   =>  $app_restaurant_image,
                    'make_it_popular'   =>  $make_it_popular,
                    'tag_detail'   =>  $tag_detail,
                    'created_by'     => $added_by ,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                $table="vendor";
                $result = $this->Adminmodel->insertRecordQueryList($table,$data);
                if($result){
                    $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Vendor Inserted</div>');
                    $url='viewVendor';
                    redirect($url);
                }
                else{
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Vendor not inserted</div>') ;
                    $url='viewVendor';
                    redirect($url);
                } 
            }
        }else {
            $this->load->view('admin/add_vendor',$dataBefore);    
        }       
    }
    
    public function editVendor(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->uri->segment('3');
        if($id==''){
            redirect('adminLogin');
        }
        $tablename = "vendor";
        $resultCategory = $this->Adminmodel->getMasterCategory('category');
        $resultCusine = $this->Adminmodel->getMasterCategory('cusine_types');
        $data['resultCat'] = $resultCategory;
        $data['resultCusine'] = $resultCusine;   
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
        if($result) {
            foreach ($result as $key => $field) {
                $data['vendor_pan_image'] = base_url()."uploads/vendor_pan_image/".$field['vendor_pan_image'];
                $data['vendor_adhar_image'] = base_url()."uploads/vendor_adhar_image/".$field['vendor_adhar_image'];
                $data['business_pan_mage'] = base_url()."uploads/business_pan_mage/".$field['business_pan_mage'];
                $data['registration_certificate_image'] = base_url()."uploads/registration_certificate_image/".$field['registration_certificate_image'];
                $data['web_logo'] = base_url()."uploads/web_logo/".$field['web_logo'];
                $data['app_logo'] = base_url()."uploads/app_logo/".$field['app_logo'];
                $data['web_restaurant_image'] = base_url()."uploads/web_restaurant_image/".$field['web_restaurant_image'];
                $data['app_restaurant_image'] = base_url()."uploads/app_restaurant_image/".$field['app_restaurant_image'];
                $catList = $this->Adminmodel->singleRecordData('vendor_code',$field['vendor_code'],'vendor_service_categories');
                if($catList) {
                    foreach ($catList as $key => $field) {
                        $catList[$key]['category_name'] = $this->Adminmodel->getSingleColumnName($field['category_id'],'id','category_name','category') ;
    
                    }
                }
                $cusineList = $this->Adminmodel->singleRecordData('vendor_code',$field['vendor_code'],'vendor_service_cusinetypes');
                if($cusineList) {
                    foreach ($cusineList as $key => $field) {
                        $cusineList[$key]['cusine_type_name'] = $this->Adminmodel->getSingleColumnName($field['cusine_type_id'],'id','cusine_type_name','cusine_types') ;
                    }
                }
            }
            $data['category'] = $catList ;
            $data['cusineType'] = $cusineList ;
            $data['result'] = $result[0] ;
            $this->load->view('admin/edit_Vendor',$data);
        } else {
            $url='viewVendor';
            redirect($url);
        }
    }
    public function updateVendor(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
        $mobile = $this->input->post('mobile');
        $vendor_code = $this->input->post('vendor_code'); 
        if($mobile!=''){            
            $check_data = array(
                "mobile" => $mobile,
                "id !=" =>$id   
            );
            $tablename = "vendor";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Vendor  already exist</div>') ;
                $url='Vendor/editVendor/'.$id;
                redirect($url);
            }else{
                if($_FILES['vendor_pan_image']['size'] > 0) {
                    $config_media['upload_path'] = './uploads/vendor_pan_image';
                    $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                    $config_media['max_size']   = '1000000000000000'; // whatever you need
                   // $this->load->library($config_media);
                    $this->upload->initialize($config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('lvendor_pan_imageogo')) {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else {
                        $data[] = array('upload_image' => $this->upload->data());
                    }       
                    $vendor_pan_image = $data[0]['upload_image']['file_name'];
                    $imgArr = array(
                        "vendor_pan_image"=>$vendor_pan_image ,
                        "updated_by"=>$added_by,
                        "updated_at"=>$date  
                    );
                    $resultUpdate = $this->Adminmodel->updateRecordQueryList('vendor',$imgArr,'id',$id);
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in vendor pan image uploads</div>') ;
                        $url='Vendor/editVendor/'.$id;
                        redirect($url);
                    }        
                }else{
                    $vendor_pan_image = "";
                }
                if($_FILES['vendor_adhar_image']['size'] > 0) {
                    $config_media1['upload_path'] = './uploads/vendor_adhar_image';
                    $config_media1['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                    $config_media1['max_size']   = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media1);
                    $error = [];
                    if ( ! $this->upload->do_upload('vendor_adhar_image'))
                    {
                        $error2[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else
                    {
                        $data1[] = array('upload_image' => $this->upload->data());
                    }       
                    $vendor_adhar_image = $data1[0]['upload_image']['file_name'];
                    $imgArr = array(
                        "vendor_adhar_image"=>$vendor_adhar_image ,
                        "updated_by"=>$added_by,
                        "updated_at"=>$date  
                    );
                    $resultUpdate = $this->Adminmodel->updateRecordQueryList('vendor',$imgArr,'id',$id);
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in vendor_adhar_image uploads</div>') ;
                        $url='Vendor/editVendor/'.$id;
                        redirect($url);
                        
                    }        
                }else{
                    $vendor_adhar_image = "";
                }
                if($_FILES['business_pan_mage']['size'] > 0) {
                    $config_media2['upload_path'] = './uploads/business_pan_mage';
                    $config_media2['allowed_types'] = 'gif|jpg|png|jpeg';   
                    $config_media2['max_size']  = '1000000000000000'; // whatever you need
                   // $this->load->library($config_media2);
                    $this->upload->initialize($config_media2);
                    $error2 = [];
                    if ( ! $this->upload->do_upload('business_pan_mage'))  {
                        $error2[] = array('error_image' => $this->upload->display_errors());    
                    }
                    else  {
                        $data2[] = array('upload_image' => $this->upload->data());
                    }       
                    $business_pan_mage = $data2[0]['upload_image']['file_name'];
                    $imgArr2 = array(
                        "business_pan_mage"=>$business_pan_mage ,
                        "updated_by"=>$added_by,
                        "updated_at"=>$date  
                    );
                    $resultUpdate = $this->Adminmodel->updateRecordQueryList('vendor',$imgArr2,'id',$id);
                    if(count($error2) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in business_pan_mage uploads</div>') ;
                        $url='Vendor/editVendor/'.$id;
                        redirect($url);
                    }        
                }else{
                    $business_pan_mage = "";
                }  
                if($_FILES['registration_certificate_image']['size'] > 0) {
                    $config_media3['upload_path'] = './uploads/registration_certificate_image';
                    $config_media3['allowed_types'] = 'gif|jpg|png|jpeg';   
                    $config_media3['max_size']  = '1000000000000000'; // whatever you need
                  //  $this->load->library($config_media3);
                    $this->upload->initialize($config_media3);
                    $error = [];
                    if ( ! $this->upload->do_upload('registration_certificate_image')) {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else {
                        $data3[] = array('upload_image' => $this->upload->data());
                    }       
                    $registration_certificate_image = $data3[0]['upload_image']['file_name'];
                    $imgArr2 = array(
                        "registration_certificate_image"=>$registration_certificate_image,
                        "updated_by"=>$added_by,
                        "updated_at"=>$date  
                    );
                    $resultUpdate = $this->Adminmodel->updateRecordQueryList('vendor',$imgArr2,'id',$id);
                    if(count($error) >0){
                       $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in registration certificate image uploads</div>') ;
                      $url='Vendor/editVendor/'.$id;
                      redirect($url);
                    }        
                }else{
                    $registration_certificate_image = "";
                }
                if($_FILES['web_logo']['size'] > 0) {
                    $config_media4['upload_path'] = './uploads/web_logo';
                    $config_media4['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                    $config_media4['max_size']   = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media4);
                    $error = [];
                    if ( ! $this->upload->do_upload('lweb_logoogo')) {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else {
                        $data4[] = array('upload_image' => $this->upload->data());
                    }       
                    $web_logo = $data4[0]['upload_image']['file_name'];
                    $imgArr = array(
                        "web_logo"=>$web_logo ,
                        "updated_by"=>$added_by,
                        "updated_at"=>$date  
                    );
                    $resultUpdate = $this->Adminmodel->updateRecordQueryList('vendor',$imgArr,'id',$id);
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in web logo uploads</div>') ;
                        $url='Vendor/editVendor/'.$id;
                        redirect($url);
                    }        
                }else{
                    $web_logo = "";
                }
                if($_FILES['app_logo']['size'] > 0) {
                    $config_media5['upload_path'] = './uploads/app_logo';
                    $config_media5['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                    $config_media5['max_size']   = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media5);
                    $error = [];
                    if ( ! $this->upload->do_upload('app_logo'))
                    {
                        $error2[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else
                    {
                        $data5[] = array('upload_image' => $this->upload->data());
                    }       
                    $app_logo = $data5[0]['upload_image']['file_name'];
                    $imgArr = array(
                        "app_logo"=>$app_logo ,
                        "updated_by"=>$added_by,
                        "updated_at"=>$date  
                    );
                    $resultUpdate = $this->Adminmodel->updateRecordQueryList('vendor',$imgArr,'id',$id);
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in app_logo uploads</div>') ;
                        $url='Vendor/editVendor/'.$id;
                        redirect($url);
                        
                    }        
                }else{
                    $app_logo = "";
                }
                if($_FILES['web_restaurant_image']['size'] > 0) {
                    $config_media6['upload_path'] = './uploads/web_restaurant_image';
                    $config_media6['allowed_types'] = 'gif|jpg|png|jpeg';   
                    $config_media6['max_size']  = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media6);
                    $error2 = [];
                    if ( ! $this->upload->do_upload('web_restaurant_image'))  {
                        $error2[] = array('error_image' => $this->upload->display_errors());    
                    }
                    else  {
                        $data6[] = array('upload_image' => $this->upload->data());
                    }       
                    $web_restaurant_image = $data6[0]['upload_image']['file_name'];
                    $imgArr2 = array(
                        "web_restaurant_image"=>$web_restaurant_image ,
                        "updated_by"=>$added_by,
                        "updated_at"=>$date  
                    );
                    $resultUpdate = $this->Adminmodel->updateRecordQueryList('vendor',$imgArr2,'id',$id);
                    if(count($error2) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in web_restaurant_image uploads</div>') ;
                        $url='Vendor/editVendor/'.$id;
                        redirect($url);
                    }        
                }else{
                    $web_restaurant_image = "";
                }  
                if($_FILES['app_restaurant_image']['size'] > 0) {
                    $config_media7['upload_path'] = './uploads/app_restaurant_image';
                    $config_media7['allowed_types'] = 'gif|jpg|png|jpeg';   
                    $config_media7['max_size']  = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media7);
                    $error = [];
                    if ( ! $this->upload->do_upload('app_restaurant_image')) {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else {
                        $data7[] = array('upload_image' => $this->upload->data());
                    }       
                    $app_restaurant_image = $data7[0]['upload_image']['file_name'];
                    $imgArr2 = array(
                        "app_restaurant_image"=>$app_restaurant_image,
                        "updated_by"=>$added_by,
                        "updated_at"=>$date  
                    );
                    $resultUpdate = $this->Adminmodel->updateRecordQueryList('vendor',$imgArr2,'id',$id);
                    if(count($error) >0){
                       $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in registration certificate image uploads</div>') ;
                      $url='Vendor/editVendor/'.$id;
                      redirect($url);
                    }        
                }else{
                    $app_restaurant_image = "";
                }
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;           
                $date     = date("Y-m-d H:i:s");
                $email = $this->input->post('email')=="" ? "":$this->input->post('email');
                $password = $this->input->post('password')=="" ? "":$this->input->post('password');
                $name = $this->input->post('name')=="" ? "":$this->input->post('name');
                $vendor_pan_number = $this->input->post('vendor_pan_number')=="" ? "":$this->input->post('vendor_pan_number');
                $vendor_adhar_number = $this->input->post('vendor_adhar_number')=="" ? "":$this->input->post('vendor_adhar_number');
                $address = $this->input->post('address')=="" ? "":$this->input->post('address');
                $business_name = $this->input->post('business_name')=="" ? "":$this->input->post('business_name');
                $restaurant_name = $this->input->post('restaurant_name')=="" ? "":$this->input->post('restaurant_name');
                $restaurant_type = $this->input->post('restaurant_type')=="" ? "":$this->input->post('restaurant_type');
                $business_pan_number = $this->input->post('business_pan_number')=="" ? "":$this->input->post('business_pan_number');
                $registration_certificate_no = $this->input->post('registration_certificate_no')=="" ? "":$this->input->post('registration_certificate_no');
                $established_year = $this->input->post('established_year')=="" ? "":$this->input->post('established_year');
                $make_it_popular = $this->input->post('make_it_popular')=="" ? "":$this->input->post('make_it_popular');
                $tag_detail = $this->input->post('tag_detail')=="" ? "":$this->input->post('tag_detail');
                //FOR Categories
                $catData = $this->Adminmodel->singleRecordData('vendor_code',$vendor_code,'vendor_service_categories');
                $category_id = $this->input->post('category_id');
                if($catData) {
                    foreach($catData as $key=>$row) {
                        $category = $this->Adminmodel->whereIn($category_id,$row['category_id'],'vendor_service_categories');
                    }
                }
                
                //print_r($category); exit;
                //FOR Vendor Login
                $vendorData = array(
                    'username' => $name,
                    'password' => $password,
                    'useremail' => $email,
                );
                $where = array(
                    'vendor_code'=> $vendor_code ,
                );
                $result = $this->Adminmodel->updateRecordQueryList2('vendor_login',$vendorData,$where);
                $data = array(
                    'name'=> $name ,                    
                    'email'=>  $email,
                    'mobile'=>  $mobile,
                    'password'=>  $password,
                    'vendor_pan_number'=>  $vendor_pan_number,
                    'vendor_adhar_number'=>  $vendor_adhar_number,
                    'address'=>  $address,
                    'business_name'=>  $business_name,
                    'restaurant_name'=>  $restaurant_name,
                    'restaurant_type'=>  $restaurant_type,
                    'business_pan_number'=>  $business_pan_number,
                    'registration_certificate_no'=>  $registration_certificate_no,
                    'established_year'=>  $established_year,
                    'vendor_code'=>  $vendor_code,
                    'make_it_popular'   =>  $make_it_popular,
                    'tag_detail'   =>  $tag_detail,
                    'created_by'     => $added_by ,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                $table="vendor";
                $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                if($result){
                    $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">Vendor Updated.</div>');
                }else{
                    $url='Vendor/editVendor/'.$id;
                    redirect($url);
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps! Some error, Vendor not updated.</div>') ;
                }   
            } 
            $url='viewVendor';
            redirect($url);
        }else {   
            $url='Vendor/editVendor/'.$id;
            redirect($url); 
        }
    }
    public function vendorDetails(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller.
        {
        redirect('admin');
        }
        $input  = json_decode(file_get_contents('php://input'), true);
        $start=0;
        $perPage = 100;
        $id = $this->uri->segment('3');
        //if($start!="" && $perPage!=""){
        $table="vendor";
        if($id !=""){
        @$column = "id";
        @$value  = $id;
        }
        $search ='';
    
        $result1 = $this->Adminmodel->get_current_page_records($table,$perPage,$start,@$column,@$value,@$search,@$searchColumn);
        $result=replace_attr($result1);
        if($result){
            foreach ($result as $key => $field) {
                $data['vendor_pan_image'] = base_url()."uploads/vendor_pan_image/".$field['vendor_pan_image'];
                $data['vendor_adhar_image'] = base_url()."uploads/vendor_adhar_image/".$field['vendor_adhar_image'];
                $data['business_pan_mage'] = base_url()."uploads/business_pan_mage/".$field['business_pan_mage'];
                $data['registration_certificate_image'] = base_url()."uploads/registration_certificate_image/".$field['registration_certificate_image'];
                $data['web_logo'] = base_url()."uploads/web_logo/".$field['web_logo'];
                $data['app_logo'] = base_url()."uploads/app_logo/".$field['app_logo'];
                $data['web_restaurant_image'] = base_url()."uploads/web_restaurant_image/".$field['web_restaurant_image'];
                $data['app_restaurant_image'] = base_url()."uploads/app_restaurant_image/".$field['app_restaurant_image'];
                $catList = $this->Adminmodel->singleRecordData('vendor_code',$field['vendor_code'],'vendor_service_categories');
                if($catList) {
                    foreach ($catList as $key => $field) {
                        $catList[$key]['category_name'] = $this->Adminmodel->getSingleColumnName($field['category_id'],'id','category_name','category') ;
    
                    }
                }
                $cusineList = $this->Adminmodel->singleRecordData('vendor_code',$field['vendor_code'],'vendor_service_cusinetypes');
                if($cusineList) {
                    foreach ($cusineList as $key => $field) {
                        $cusineList[$key]['cusine_type_name'] = $this->Adminmodel->getSingleColumnName($field['cusine_type_id'],'id','cusine_type_name','cusine_types') ;
                    }
                }
            }
            $data['category'] = $catList ;
            $data['cusineType'] = $cusineList ;
            $data['result'] = $result[0] ;
            $this->load->view('admin/vendor_Details',$data);
        }
        else{
        $url='viewVendor';
        redirect($url);
        }
    }
    function VendorEnable($id) {
        $id=$id;
        $vendorData =array(
            'isactive' =>'0'
        );
        $table="vendor";
        $result = $this->Adminmodel->updateRecordQueryList($table,$vendorData,'id',$id);
        $url='viewVendor';
        redirect($url);
    }      
    function VendorDisable($id) {
        $id=$id;
        $vendorData =array(
            'isactive' =>'1'
        );
        $table="vendor";
        $result = $this->Adminmodel->updateRecordQueryList($table,$vendorData,'id',$id);
        $url='viewVendor';
        redirect($url);
    }
    function deleteVendor($id) {
        $id=$id;
        $result = $this->Adminmodel->delmultipleImage($id,'vendor','vendor_pan_image','vendor_adhar_image','vendor_pan_image','vendor_adhar_image','id');
        $result = $this->Adminmodel->delmultipleImage($id,'vendor','business_pan_mage','registration_certificate_image','business_pan_mage','registration_certificate_image','id');
        $result = $this->Adminmodel->delmultipleImage($id,'vendor','web_logo','app_logo','web_logo','app_logo','id');
        $result = $this->Adminmodel->delmultipleImage($id,'vendor','web_restaurant_image','app_restaurant_image','web_restaurant_image','app_restaurant_image','id');
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }
}
?>
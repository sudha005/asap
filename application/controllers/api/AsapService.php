<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH . '/libraries/REST_Controller.php');

/**
 * Description of RestPostController
 * 
 */
class AsapService  extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->helper("encryptionpwd");
        $this->load->helper("sms");
        $this->load->model('Adminmodel');
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
       
    }

     /*
    Register   servive 
    parameters--> fname,lname,username,email,mobile,password,gender
    method--> post
    
    */
    public function registerUser_post(){
        $input         = json_decode(file_get_contents('php://input'), true); 
        if(!empty($input['mobile']) && !empty($input['reg_type']) ){
            $mobile        = $input['mobile'];
			$name        = $input['name'];
			$email        = $input['email'];
			$password        = encryptPassword($input['password']);
            $date_join     = date('Y-m-d H:i:s');
            $auth_key      = bin2hex(openssl_random_pseudo_bytes(16));            
            $random1       = rand(102,8596);
            $string2       = str_shuffle('1026358132659');
            $random2       = substr($string2,0,3);
            //$contstr       = "ASAP";
            $userId        = $random1.$random2;
            $regType       = $input['reg_type']; 
            $userCheck     = array(
                'user_mobile'  => $mobile
            );
			$user_login_type_id=1;
            $data = array(
            'user_code'         => $userId,
            'user_token'        => $auth_key,
            'user_name'         => $name ,
            'user_email'        => $email ,             			
            'user_mobile'       => $mobile ,
			'user_password'     => $password,
            'user_createdat'        => $date_join,
            'register_devicetype_id'=>$regType,
			'user_login_type_id'  => $user_login_type_id,
			'user_isactive' => '1',
			
            );
            $mob="/^[1-9][0-9]*$/";
            if(!preg_match($mob,$input['mobile']) || strlen($input['mobile'])!='10'){
                $json_data['status']='FALSE';
                $json_data['responseCode']=2;
                $json_data['message']="Invalid Mobile Number"; 
                $this->set_response($json_data,REST_Controller::HTTP_OK);
            }elseif($regType !='2' && $regType !='3'){
                $json_data['status']='FALSE'.$regType;
                $json_data['responseCode']=7;
                $json_data['message']="Enter Correct Register Type 1 for android 2 for ios"; 
                $this->set_response($json_data,REST_Controller::HTTP_OK);
            }
            else{
                $checkData = $this->Adminmodel->userExist($userCheck) ;
                if($checkData >0){
                    $json_data['status']='FALSE';
                    $json_data['responseCode']=6;
                    $json_data['message']="Mobile Number Already Exist"; 
                    $this->set_response($json_data,REST_Controller::HTTP_OK);
                }else{               
                    $checkData = $this->Adminmodel->regUser($data) ;
                    $json_data['status']='TRUE';
                    $json_data['responseCode']=0;
                    $json_data['message']="success register"; 
                    $this->set_response($json_data,REST_Controller::HTTP_OK);
                }
            }
        }else{
            $json_data['status']='FALSE';
            $json_data['responseCode']=4;
            $json_data['message']="Please enter all the mandatory fields"; 
            $this->set_response($json_data,REST_Controller::HTTP_OK);
        } 
    }
    /*
    Login servive 
    parameters--> username , password
    method-->post
    */
   public function loginUser_post(){        
        $input =json_decode(file_get_contents('php://input'), true);         
        if(isset($input['mobile']) && !empty($input['password']) ){
            $mob="/^[1-9][0-9]*$/";
            if(!preg_match($mob,$input['mobile']) || strlen($input['mobile'])!='10') {
                $json_data['status']='FALSE';
                $json_data['responseCode']=2;
                $json_data['message']="Invalid Mobile Number"; 
                $this->set_response($json_data,REST_Controller::HTTP_OK);
            }else{
                $data = array(
                'user_mobile'   => $input['mobile'],
                'user_password' => encryptPassword($input['password']),
                );  
                $result = $this->Adminmodel->userLogin($data,$input['mobile']);
                if($result)
                {
                    $json_data['status']='TRUE';
                    $json_data['responseCode']=0;
                    $json_data['message']="login success";
                    $userlist=$result; 
                    $json_data['userDetail']=$userlist[0];
                    $this->set_response($json_data,REST_Controller::HTTP_OK); 
                }
                else
                {
                    $json_data['status']='FALSE';
                    $json_data['responseCode']=1;
                    $json_data['message']="login fail";
                    $this->set_response($json_data,REST_Controller::HTTP_OK); 
                }  
            }
        }else{
            $json_data['status']='FALSE';
            $json_data['responseCode']=4;
            $json_data['message']="Please enter all the mandatory fields"; 
            $this->set_response($json_data,REST_Controller::HTTP_OK);
        }
    }
    public function fogetPassword_post(){
        $input = json_decode(file_get_contents('php://input'), true);  
        $email = $input['email'];
        $result = $this->Adminmodel->forgetPwd($email);
        if($result){
            $json_data['status']='Success';
            $json_data['responseCode']=array('115');
            $json_data['message']="Password recovery successfully.";
            $data=$result; 
            $json_data['credentialDetail']=$data;
            $this->set_response($json_data,REST_Controller::HTTP_OK); 
        }
        else{
            $json_data['status']='Fail';
            $json_data['responseCode']=array('116');
            $data=$email; 
            $json_data['credentialDetail']=$data;
            $json_data['message']="Records not found";
            $this->set_response($json_data,REST_Controller::HTTP_OK);
        }
    }
	
	//User profile 
	public function userProfile_post(){
        $input = json_decode(file_get_contents('php://input'), true);  
        $user_code = $input['user_code'];
		$user_token = $input['user_token'];
		if(!empty($user_code) && !empty($user_token)){
			$checkToken = $this->Adminmodel->checkUserAuthKey($user_code,$user_token);
			if($checkToken > 0){				
				$result = $this->Adminmodel->getUsers($user_code,$checkToken);
				if($result){
					$json_data['status']='Success';
					$json_data['responseCode']=0;
					$json_data['message']="Profile Retrive successfully.";
					$data=$result; 
					$json_data['credentialDetail']=$data;
					$this->set_response($json_data,REST_Controller::HTTP_OK); 
				}
				else{
					$json_data['status']='Fail';
					$json_data['responseCode']=1;
					$json_data['message']="Records not found";
					$this->set_response($json_data,REST_Controller::HTTP_OK);
				}
			}else{
					$json_data['status']='Fail';
					$json_data['responseCode']=7;
					$json_data['message']="Invalid Token";
					$this->set_response($json_data,REST_Controller::HTTP_OK);
			}
		}else{
				$json_data['status']='Fail';
				$json_data['responseCode']=4;				
				$json_data['message']="Please enter all mandatory parameters";
				$this->set_response($json_data,REST_Controller::HTTP_OK);
		}
    }
	
	
	
	//update  profile 
	public function updateProfile_post(){
        $input = json_decode(file_get_contents('php://input'), true);  
        $user_code = $input['user_code'];
		$user_token = $input['user_token'];
		if(!empty($user_code) && !empty($user_token)){
			$checkToken = $this->Adminmodel->checkUserAuthKey($user_code,$user_token);
			if($checkToken > 0){
                $name   = $input['user_code'];
			    $mobile = $input['mobile'];
				$mobile = $input['mobile'];
				$mobile = $input['mobile'];
				$mobile = $input['mobile'];
				$mobile = $input['mobile'];
				
				$result = $this->Adminmodel->getUsers($user_code,$checkToken);
				if($result){
					$json_data['status']='Success';
					$json_data['responseCode']=0;
					$json_data['message']="Profile Retrive successfully.";
					$data=$result; 
					$json_data['credentialDetail']=$data;
					$this->set_response($json_data,REST_Controller::HTTP_OK); 
				}
				else{
					$json_data['status']='Fail';
					$json_data['responseCode']=1;
					$json_data['message']="Records not found";
					$this->set_response($json_data,REST_Controller::HTTP_OK);
				}
			}else{
					$json_data['status']='Fail';
					$json_data['responseCode']=7;
					$json_data['message']="Invalid Token";
					$this->set_response($json_data,REST_Controller::HTTP_OK);
			}
		}else{
				$json_data['status']='Fail';
				$json_data['responseCode']=4;				
				$json_data['message']="Please enter all mandatory parameters";
				$this->set_response($json_data,REST_Controller::HTTP_OK);
		}
    }
	
	
	
	
	
	
    // insert query
    public function getUserDtail($id){
        $this->db->where('user_mobile',$id);
        $query = $this->db->get('users');
        $result = $query->result();
        if($result){
            return $result[0];
        }
        else{
            return false ;
        }
    }
    // for chnage password 
    public function changePassword_post(){
        $input = json_decode(file_get_contents('php://input'), true); 
        $userId=$input['userId'];
        $userAuthKey=$input['userAuthKey'];
        if(isset($userId) && isset($userAuthKey) && (self::checkUserAuthKey($userAuthKey,$userId) > 0)) {

            @$username     = $input['username'];
            @$old_password = $input['old_password'];
            @$new_password =$input['new_password'];
            @$confirm_password = $input['new_password'];
            if($old_password=='' || $new_password==''){
                $json_data['status']='5';
                $json_data['responseCode']=array('5');
                $json_data['message']="Please enter mandatory fileds ";
                $this->set_response( $json_data,REST_Controller::HTTP_OK);     
                
            }
            elseif($new_password!=$confirm_password){
                $json_data['status']='4';
                $json_data['responseCode']=array('4');
                $json_data['message']="New and confirm password should be match";
                $this->set_response($json_data,REST_Controller::HTTP_OK);      
            }
             elseif($new_password==$old_password){
                $json_data['status']='3';
                $json_data['responseCode']=array('3');
                $json_data['message']="New and old password should not  be same";
                $this->set_response($json_data,REST_Controller::HTTP_OK);      
            }
            else{
                    $data = array(
                        'userId' => $userId ,
                        'user_password' => encryptPassword($old_password),
                        
                        );
                    $data1 = array(
                        'userId' => $userId ,
                        'user_password' => encryptPassword($new_password),
                        );    
                    $result = $this->Adminmodel->checkPassword($data,$data1);
                    if($result){
                        $json_data['status']='0';
                        $json_data['responseCode']=array('0');
                        $json_data['message']="password changed";
                        $this->set_response($json_data,REST_Controller::HTTP_OK); 
                    }
                    else{
                    $json_data['status']='FALSE';
                    $json_data['responseCode']=array(1);
                    $json_data['message']="Invalid Old Password";
                    $this->set_response( $json_data,REST_Controller::HTTP_OK);      
                
                }
                
                
            }
        }else{
            $json_data['status']='FALSE';
            $json_data['responseCode']=array(2);
            $json_data['message']="Inalid Auth Key";
            $this->set_response($json_data,REST_Controller::HTTP_OK); 
        }
        
    }
   // for send otp 
    public function sendOtp_post(){
        $input = json_decode(file_get_contents('php://input'), true); 
        if (isset($input['mobile']) && !empty($input['mobile']) && !empty($input['device_type']) && !empty($input['otp_type'])){
            
            $mob="/^[1-9][0-9]*$/";
			$device_type = $input['device_type'];
			$otp_type    = $input['otp_type'];
			
            if(!preg_match($mob,$input['mobile']) || strlen($input['mobile'])!='10') {
                $json_data['status']='FALSE';
                $json_data['responseCode']=2;
                $json_data['message']="Invalid Mobile Number"; 
                $this->set_response($json_data,REST_Controller::HTTP_OK);
            }else{
                $min = "1230";
                $max = "5269";
                $rand1=rand($min,$max);
				// update
                $data1 = array(
                    'user_otp' =>$rand1,                   
                );
                // for checking  				
                $where = array(
				    'otp_mobile' =>$input['mobile'],  
                    'otp_type'   =>$otp_type,  
                    'otp_device_type_id'=>$device_type 					
                );
                // for insert				
                $data2 = array(
				    'otp_mobile' =>$input['mobile'],
                    'user_otp' =>$rand1, 					
                    'otp_type'   =>$otp_type,  
                    'otp_device_type_id'=>$device_type 					
                ); 					
                $message1 = urlencode('OTP from asap is '.$rand1.' . Do not share it with any one.'); // Message text required to deliver on mobile number
               //$sendSMS = sendMobileSMS($message1,$mobile);
                $result = $this->Adminmodel->userOtp($where,$data1,$data2);
                if($result){                    
                    $json_data['status']='TRUE';
                    $json_data['responseCode']=0;
                    $json_data['message']="OTP Sent successfully.".$rand1;
                    $this->set_response($json_data,REST_Controller::HTTP_OK); 
                }
                else{
                    $json_data['status']='FALSE';
                    $json_data['responseCode']=1;
                    $data=$email; 
                    $json_data['credentialDetail']=$data;
                    $json_data['message']="Records not found";
                    $this->set_response($json_data,REST_Controller::HTTP_OK);
                }
                }  
            }else{
                $json_data['status']='FALSE';
                $json_data['responseCode']=4;               
                $json_data['message']="Please enter all the mandatory fields";
                $this->set_response($json_data,REST_Controller::HTTP_OK);
            }
    
    }    
    // for verify  otp 
    public function verifyOtp_post(){
        $input = json_decode(file_get_contents('php://input'), true);  
        if (isset($input['mobile']) && !empty($input['mobile']) && isset($input['otp']) && !empty($input['otp']) && !empty($input['device_type']) && !empty($input['otp_type'])){ 
            $mob="/^[1-9][0-9]*$/";
            if(!preg_match($mob,$input['mobile']) || strlen($input['mobile'])!='10') {
                $json_data['status']='FALSE';
                $json_data['responseCode']=2;
                $json_data['message']="Invalid Mobile Number"; 
                $this->set_response($json_data,REST_Controller::HTTP_OK);
            }else{
               
                $otpCheck     = array(
                    'otp_mobile'         => $input['mobile'],
                    'user_otp'           => $input['otp'],
					'otp_type'           => $input['otp_type'],
					'otp_device_type_id' => $input['device_type'],
                );
                $result = $this->Adminmodel->userVerifyOtp($otpCheck);
                if($result){
                    $json_data['status']='TRUE';
                    $json_data['responseCode']=0;
                    $json_data['message']="Thank You . OTP Verified.";
                    $this->set_response($json_data,REST_Controller::HTTP_OK); 
                }
                else{
                    $json_data['status']='FALSE';
                    $json_data['responseCode']=6;
                    $json_data['message']="Sorry ! Your entered Invalid OTP !.";
                    $this->set_response($json_data,REST_Controller::HTTP_OK);
                } 
            }
        }else{
            $json_data['status']='FALSE';
            $json_data['responseCode']=4;               
            $json_data['message']="Please enter all the mandatory fields";
            $this->set_response($json_data,REST_Controller::HTTP_OK);
        }
    }  
    public function resetPassword_post(){
        $input = json_decode(file_get_contents('php://input'), true); 
        if(isset($input['mobile'])){
            $mobile   = $input['mobile'];
            $password = $input['password'];
            $userCheck = array(
                'user_mobile' => $mobile
            );
            $checkData = $this->Adminmodel->userExist($userCheck);
            if($checkData >0){
                $data1 = array(
                    'user_password' => encryptPassword($password),
                );    
                $result = $this->Adminmodel->resetPassword($mobile,$data1); 
                if($result){
                    $json_data['status']='TRUE';
                    $json_data['responseCode']=0;
                    $json_data['message']="success";
                    $json_data['userlist']=$result[0];
                    $this->set_response( $json_data,REST_Controller::HTTP_OK); 
                }else{
                    $json_data['status']='FALSE';
                    $json_data['responseCode']=array('3');
                    $json_data['message']="Opps some errors";
                    $this->set_response( $json_data,REST_Controller::HTTP_OK); 
                }
            }
            else{
                $json_data['status']='FALSE';
                $json_data['responseCode']=array(1);
                $json_data['message']="Mobile Number Not Register With Us";
                $this->set_response( $json_data,REST_Controller::HTTP_OK);      
            }
        }else{
            $json_data['status']='FALSE';
            $json_data['responseCode']='4';
            $json_data['message']="Please enter mandatory fileds";
            $this->set_response($json_data,REST_Controller::HTTP_OK); 
        }        
    }
    public function categories_post(){
        $result = $this->Adminmodel->categoryList();
        if($result){
            $json_data['status']='Success';
            $json_data['responseCode']=0;
            $json_data['message']="Category List Retrive Successfully.";
            $data=$result; 
            $json_data['list']=$data;
            $this->set_response($json_data,REST_Controller::HTTP_OK); 
        }
        else{
            $json_data['status']='Fail';
            $json_data['responseCode']=1;
            $json_data['message']="Records not found";
            $this->set_response($json_data,REST_Controller::HTTP_OK);
        }
    }
    //  resturant list 
    public function restaurantList_post(){
        $input = json_decode(file_get_contents('php://input'), true); 
        $userId      = $input['userId'];
        $userAuthKey = $input['userAuthKey'];
        $userId ='OMLY_tak954';
        $userAuthKey = 'a8c48a1db836eb0e6b048fd629a0a9f3';        
        $lat         = $input['lat'];
        $long        = $input['long'];
        $distance    =  self::getSettingDetail('maxDistance') ;
        $chefType    = 1;
        $checkPoint  = 20;// else cond
        $minLimit    = 0;
        $maxLimit    = 100;
        if(isset($userId) && isset($userAuthKey) && (self::checkUserAuthKey($userAuthKey,$userId) > 0)  && isset($lat) && isset($long)) {
           $result2 = $this->Adminmodel->restaurantList($lat,$long,$checkPoint,$chefType,$minLimit,$maxLimit,$distance);
           $list = array();
           if($result2){
            $k=0;
            foreach ($result2 as $key => $field) {               
                    $listdada=$this->Adminmodel->restaurantBranchList($lat,$long,$field['vendorId'],$distance) ; 
                    if($listdada !="0"){                       
                        if(!array_key_exists($listdada[$k]['vendorId'], $list))
                        {        
                           $ard = array("outlet"=>1);
                           $listdada=array_merge($listdada,$ard);
                           array_push($list,$listdada);
                        }                           
                    }
                    $k++;
                    $list[$key]['outlet'] = self::countOutlet($lat,$long,$field['vendorId'],$distance);  
            } 
                $json_data['status']='Success';
                $json_data['responseCode']=0;
                $json_data['message']="Restaurant List Retrive Successfully.";
                $json_data['list']=$list;
                $this->set_response($json_data,REST_Controller::HTTP_OK); 
            }
            else{
                $json_data['status']='Fail';
                $json_data['responseCode']=1;
                $json_data['message']="Records not found";
                $this->set_response($json_data,REST_Controller::HTTP_OK);
            }
        } else{
                $json_data['status']='FALSE';
                $json_data['responseCode']=4;               
                $json_data['message']="Please enter all the mandatory fields";
                $this->set_response($json_data,REST_Controller::HTTP_OK);
        }           
    }
    
    // for  restaurant itemList 
    
    public function restaurantItemList_post(){
        $input = json_decode(file_get_contents('php://input'), true); 
        $userId      = $input['userId'];
        $userAuthKey = $input['userAuthKey'];
        $userId ='OMLY_tak954';
        $userAuthKey = 'a8c48a1db836eb0e6b048fd629a0a9f3';        
        $vendorId = $input['vendorId'];
        $branchId = $input['branchId'];
        $catId    = $input['catId'];
        $dishType = $input['dishType'];
        $search   = $input['search'];  
        
        if(isset($userId) && isset($userAuthKey) && (self::checkUserAuthKey($userAuthKey,$userId) > 0)  && isset($vendorId) && isset($branchId)) {
           $result = $this->Adminmodel->restaurantItemList($vendorId,$branchId,$catId,$dishType,$search);
          
           if($result){
            
                 foreach ($result as $key => $field) {
                       
                        $result[$key]['productImage'] = base_url().'uploads/productImage/'.$field['productImage'];  
                        $result[$key]['addOnproductList'] =  $this->Adminmodel->productAddon($branchId,$field['productId']); 
                        
                          
                }
                $json_data['status']='Success';
                $json_data['responseCode']=0;
                $json_data['message']="Restaurant Item List Retrive Successfully.";
                $json_data['list']=$result;
                $this->set_response($json_data,REST_Controller::HTTP_OK); 
            }
            else{
                $json_data['status']='Fail';
                $json_data['responseCode']=1;
                $json_data['message']=array("Records not found");
                $this->set_response($json_data,REST_Controller::HTTP_OK);
            }
        } else{
                $json_data['status']='FALSE';
                $json_data['responseCode']=4;               
                $json_data['message']="Please enter all the mandatory fields";
                $this->set_response($json_data,REST_Controller::HTTP_OK);
        }           
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // setting detail 
    public function getSettingDetail($column){
        $this->db->where('id','1');
        $query = $this->db->get('tblbasicsitesettings');
        $result = $query->row();
        if($result){
            return $result->$column;
        }
        else{
            return "" ;
        }
    }
    
    public function countOutlet($lat,$long,$venid,$distance){
        $qyery= $this->db->query("CALL uspGetAllVendorBranches($lat,$long,$venid,$distance)");
        $result=$qyery->num_rows();
        if($result > 0){
            return $result;
        }
        else{
            return 1;
        }
    }
}   
?>
<?php
include_once'header.php';
?>
<div class="page-content-wrapper ">
	<div class="container-fluid">
		<div class='row'>  
			<div class="col-md-12 col-xl-12">
				<div class="card m-b-30 m-t-30">
					<div class="card-body">
						<h4 class="mt-0 header-title">Branch Detail :<?php echo $result['username'] ?><a href="<?php echo base_url(); ?>Branch/editBranch/<?php echo $result['id'] ?>"><i class="mdi mdi-lead-pencil"></i></a></h4>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-6">
								<div class="card m-b-30">
                                            <h6 class="card-header">
                                                Basic Detail
                                            </h6>
                                        <div class="card-body">
											<table class="table mb-0">												
												<tbody>
													<tr class='border-0'>
														<td class='border-0'>Name</td>
														<td class='border-0'><?php echo $result['username'] ?></td>	
													</tr>
													<tr>	
														<td>Password</td>
														<td><?php echo $result['password'] ?></td>
													</tr>
													<tr>	
														<td>Branch Address</td>
														<td><?php echo $result['branch_adress'] ?></td>	
													</tr>
                                                    <tr>	
														<td>Latitude</td>
														<td><?php echo $result['latitude'] ?></td>	
													</tr>
                                                    <tr>	
														<td>Longitude</td>
														<td><?php echo $result['longitude'] ?></td>	
													</tr>
												</tbody>
											</table>                                             
                                        </div> 
                                    </div> 
								</div>
								<div class="col-md-6">
									<div class="card m-b-30">
                                            <h6 class="card-header">
                                                Business Information
                                            </h6>
                                        <div class="card-body">
											<table class="table mb-0">
												<tbody>
													<tr class='border-0'>	
														<td class='border-0'>Delivery Time</td>
														<td class='border-0'><?php echo $result['delivery_time'] ?></td>
													</tr>
													<tr>	
														<td>Minimum Order</td>
														<td><?php echo $result['min_order'] ?></td>	
													</tr>
                                                    <tr>	
														<td>Delivery Charges</td>
														<td><?php echo $result['delivery_charges'] ?></td>	
													</tr>
                                                    <tr>	
														<td>Opening Time</td>
														<td><?php echo $result['opening_time'] ?></td>	
													</tr>
                                                    <tr>	
														<td>Closing Time</td>
														<td><?php echo $result['closing_time'] ?></td>	
													</tr>
												</tbody>
											</table>                                             
                                        </div> 
                                    </div> 
								</div>
								<div class="col-md-12">
									<div class="card m-b-30">
                                            <h6 class="card-header">
                                                Account Detail Information
                                            </h6>
                                        <div class="card-body">
                                            <table class="table mb-0">
												<tbody>
													<tr class='border-0'>	
														<td class='border-0'>Delivery Time</td>
														<td class='border-0'><?php echo $result['bank_name'] ?></td>
													</tr>
													<tr>	
														<td>Account Number</td>
														<td><?php echo $result['account_number'] ?></td>	
													</tr>
                                                    <tr>	
														<td>IFSC Code</td>
														<td><?php echo $result['ifsc_code'] ?></td>	
													</tr>
                                                    <tr>	
														<td>Bank Branch Name</td>
														<td><?php echo $result['bank_branch_name'] ?></td>	
													</tr>
                                                    <tr>	
														<td>Bank Address</td>
														<td><?php echo $result['bank_address'] ?></td>	
													</tr>
                                                    <tr>	
														<td>Cancelled Cheque</td>
														<td><img src="<?php echo $cancelled_cheque ?>" alt="Cancelled Cheque" style="width:250px; height:250px"></td>	
													</tr>
												</tbody>
											</table> 
                                        </div> 
                                    </div> 
								</div>	
							</div>
						</div>
						
						
						
						
					</div>
				</div>
			</div> <!-- end col -->
		</div> <!-- end row -->
								
	</div>
</div>
<?php
include_once'footer.php';
?>
                                       
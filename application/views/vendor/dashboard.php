<?php
include_once'header.php';
?>
<div class="page-content-wrapper ">
    <div class="container-fluid">
        <div class='row'>  
            <div class="col-md-12 col-xl-12">
                <div class="card m-b-30 m-t-30">
                    <div class="card-body">
                         <!-- end page title end breadcrumb -->
                            <div class="row">
                                <!-- Column -->
                                <div class="col-sm-12 col-md-6 col-xl-3">
                                    <div class="card bg-danger m-b-30">
                                        <a href="<?php echo base_url() ?>viewBranch">
                                            <div class="card-body">
                                                <div class="d-flex row">
                                                    <div class="col-3 align-self-center">
                                                        <div class="round">
                                                            <i class="mdi mdi-google-physical-web"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-8 ml-auto align-self-center text-center">
                                                        <div class="m-l-10 text-white float-right">
                                                            <h5 class="mt-0 round-inner"><?php echo $result['branch']; ?></h5>
                                                            <p class="mb-0 ">Branch</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <!-- Column -->
                                <!-- Column -->
                                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-3">
                                    <div class="card bg-info m-b-30">
                                        <a href="<?php echo base_url() ?>viewVendorproducts">
                                            <div class="card-body">
                                                <div class="d-flex row">
                                                    <div class="col-3 align-self-center">
                                                        <div class="round">
                                                            <i class="mdi mdi-account-multiple-plus"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-8 text-center ml-auto align-self-center">
                                                        <div class="m-l-10 text-white float-right">
                                                            <h5 class="mt-0 round-inner"><?php echo $result['vendorproducts']; ?></h5>
                                                            <p class="mb-0 ">Vendor Products</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <!-- Column -->
                                <!-- Column -->
                                <!-- <div class="col-sm-12 col-md-6 col-lg-6 col-xl-3">
                                    <div class="card bg-success m-b-30">
                                        <a href="<?php echo base_url() ?>viewVendor">
                                            <div class="card-body">
                                                <div class="d-flex row">
                                                    <div class="col-3 align-self-center">
                                                        <div class="round ">
                                                            <i class="mdi mdi-basket"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-8 ml-auto align-self-center text-center">
                                                        <div class="m-l-10 text-white float-right">
                                                            <h5 class="mt-0 round-inner"><?php echo $result['vendor']; ?></h5>
                                                            <p class="mb-0 ">Vendor</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div> -->
                                <!-- Column -->
                                <!-- Column -->
                                <!-- <div class="col-sm-12 col-md-6 col-lg-6 col-xl-3">
                                    <div class="card bg-primary m-b-30">
                                        <div class="card-body">
                                            <div class="d-flex row">
                                                <div class="col-3 align-self-center">
                                                    <div class="round">
                                                        <i class="mdi mdi-calculator"></i>
                                                    </div>
                                                </div>
                                                <div class="col-8 ml-auto align-self-center text-center">
                                                    <div class="m-l-10 text-white float-right">
                                                        <h5 class="mt-0 round-inner">$32874</h5>
                                                        <p class="mb-0">Total Sales</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <!-- Column -->
                            </div>
                            <!-- <div class="row">
                                
                                <div class="col-md-12 col-xl-5">
                                    <div class="card m-b-30 h-360">
                                        <div class="card-body">
                                            <h5 class="header-title mt-0">Order Status </h5>
                                            <div class="row">
                                                <div class="col-6 align-self-center text-center">
                                                    <h6 class="text-muted">Todays Perfomance</h6>
                                                </div>
                                                <div class="col-6 align-self-center text-center">
                                                    <h6 class="font-40">
                                                        <i class="mdi mdi-menu-up text-success"></i>52 %</h6>
                                                </div>
                                            </div>
                                            <div class="text-center mt-4">
                                                <span class="float-right">18%</span>
                                                <span class="badge badge-boxed  badge-danger text-center mb-2">Delivered</span>
                                                <span class="float-left">82%</span>
            
                                                <div class="progress mt-1">
                                                    <div class="progress-bar" role="progressbar" style="width: 82%" aria-valuenow="82" aria-valuemin="0" aria-valuemax="100"></div>
                                                    <div class="progress-bar bg-danger" role="progressbar" style="width: 18%" aria-valuenow="18" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="text-center mt-4">
                                                <span class="float-right">45%</span>
                                                <span class="badge badge-boxed  badge-success mb-2">Shipped</span>
                                                <span class="float-left">55%</span>
            
                                                <div class="progress mt-1">
                                                    <div class="progress-bar" role="progressbar" style="width: 55%" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
                                                    <div class="progress-bar bg-danger" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="text-center mt-4">
                                                <span class="float-right">70%</span>
                                                <span class="badge badge-boxed badge-warning text-center mb-2">Pending</span>
                                                <span class="float-left">30%</span>
            
                                                <div class="progress mt-1">
                                                    <div class="progress-bar" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                                                    <div class="progress-bar bg-danger" role="progressbar" style="width: 70%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
            
                                            <div class="text-center mt-4">
                                                <span class="float-right text-danger">Late</span>
                                                <span class="float-left text-primary">On Time</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                            </div> -->
                            
                            
                        
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
                                
    </div>
</div>
<?php
include_once'footer.php';
?>
                                       
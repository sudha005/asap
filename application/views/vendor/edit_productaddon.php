<?php
include_once'header.php';
?>
<div class="page-content-wrapper ">
    <div class="container-fluid">
        <div class='row'>  
            <div class="col-md-12 col-xl-12">
                <div class="card m-b-30 m-t-30">
                    <div class="card-body">
                        <h4 class="mt-0 header-title">Edit Product Addon</h4>                 
                        <form action= "<?php echo base_url() ?>Productaddon/updateProductaddon" method="POST" enctype="multipart/form-data" class="mb-0">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                <label for="Addonname" class="bmd-label-floating">Addon Name</label>
                                <input type="text" class="form-control" name="addon_name" value="<?php echo $result['addon_name']; ?>" required>
                                </div>
                                <div class="form-group col-md-6">
                                <label for="Addondescription" class="bmd-label-floating">Addon Description</label>
                                <textarea id="textarea" class="form-control" name="addon_description" maxlength="225" rows="3"><?php echo $result['addon_description']; ?></textarea>
                                </div>
                            </div>
                            <input type="hidden" value="<?php echo $result['id']; ?>" name="id">
                            <button type="submit" class="btn btn-raised btn-primary mb-0">Submit</button>
                        </form>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
                                
    </div>
</div>
<?php
include_once'footer.php';
?>
                                       
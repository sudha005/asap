<?php
include_once'header.php';
?>
<div class="page-content-wrapper ">
	<div class="container-fluid">
		<div class='row'>  
			<div class="col-md-12 col-xl-12">
				<div class="card m-b-30 m-t-30">
					<div class="card-body">
						<h4 class="mt-0 header-title">Vendor Detail :<?php echo $result['name'] ?><!--<a href="<?php echo base_url(); ?>Vendor/editVendor/<?php echo $result['id'] ?>"><i class="mdi mdi-lead-pencil"></i></a>--></h4>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-6">
								<div class="card m-b-30">
                                            <h6 class="card-header">
                                                Basic Detail
                                            </h6>
                                        <div class="card-body">
											<table class="table mb-0">												
												<tbody>
													<tr class='border-0'>
														<td class='border-0'>Name</td>
														<td class='border-0'><?php echo $result['name'] ?></td>	
													</tr>
													<tr>	
														<td>Email</td>
														<td><?php echo $result['email'] ?></td>
													</tr>
													<tr>	
														<td>mobile</td>
														<td><?php echo $result['mobile'] ?></td>	
													</tr>
                                                    <tr>	
														<td>Pan Card Number</td>
														<td><?php echo $result['vendor_pan_number'] ?></td>	
													</tr>
                                                    <tr>	
														<td>Adhar Crad Number</td>
														<td><?php echo $result['vendor_adhar_number'] ?></td>	
													</tr>
                                                    <tr>	
														<td>Address</td>
														<td><?php echo $result['address'] ?></td>	
													</tr>
                                                    <tr>	
														<td>Tag Details</td>
														<td><?php echo $result['tag_detail'] ?></td>	
													</tr>
												</tbody>
											</table>                                             
                                        </div> 
                                    </div> 
								</div>
								<div class="col-md-6">
									<div class="card m-b-30">
                                            <h6 class="card-header">
                                                Business Information
                                            </h6>
                                        <div class="card-body">
											<table class="table mb-0">												
												<tbody>
													<tr class='border-0'>
														<td class='border-0'>Business Name</td>
														<td class='border-0'><?php echo $result['business_name'] ?></td>	
													</tr>
													<tr>	
														<td>Restaurant Name</td>
														<td><?php echo $result['restaurant_name'] ?></td>
													</tr>
													<tr>	
														<td>Business Pan Number</td>
														<td><?php echo $result['business_pan_number'] ?></td>	
													</tr>
                                                    <tr>	
														<td>Registration Certificate No</td>
														<td><?php echo $result['registration_certificate_no'] ?></td>	
													</tr>
                                                    <tr>	
														<td>Established Year</td>
														<td><?php echo $result['established_year'] ?></td>	
													</tr>
                                                    <tr>	
														<td>Popular</td>
														<td><?php if($result['make_it_popular'] == 0) {echo "Yes"; } elseif($result['make_it_popular'] == 1) {echo "No"; } ?></td>	
													</tr>
                                                    <tr>	
														<td>Categories</td>
														<td>
                                                        <?php   
                                                            if($category != 0 || $category != '') {
                                                                $count = count(array_filter($category));
                                                                if($count > 0) {
                                                                    $i=0;
                                                                    foreach($category as $key => $row){
                                                                        echo  $row['category_name']." ";
                                                                    }
                                                                }
                                                            } else { echo "--"; }
                                                        ?>
                                                        </td>	
													</tr>
                                                    <tr>	
														<td>Cusine Types</td>
														<td>
                                                        <?php 
                                                            if($cusineType != 0 || $cusineType != '') {
                                                                $count = count(array_filter($cusineType));
                                                                if($count > 0) {
                                                                $i=0;
                                                                foreach($cusineType as $key => $row){
                                                                    echo  $row['cusine_type_name']." ";
                                                                }
                                                                }
                                                            } else { echo "--"; }
                                                        ?>
                                                        </td>	
													</tr>
												</tbody>
											</table>                                             
                                        </div> 
                                    </div> 
								</div>
								<div class="col-md-12">
									<div class="card m-b-30">
                                            <h6 class="card-header">
                                                Media Detail Information
                                            </h6>
                                        <div class="card-body">
											<div class='row'> 
												<div class='col-md-4'>
                                                    Vendor Pan Image
													<img class="rounded mr-2 mx-auto d-block img-fluid" alt="200x200" src="<?php echo $vendor_pan_image; ?>" data-holder-rendered="true" style="width:200px; height:200px">
												</div>
												<div class='col-md-4'>
                                                    Vendor Adhar Image
													<img class="rounded mr-2 mx-auto d-block img-fluid" alt="200x200" src="<?php echo $vendor_adhar_image; ?>" data-holder-rendered="true" style="width:200px; height:200px">
												</div>
												<div class='col-md-4'>
                                                    Business Pan Image
													<img class="rounded mr-2 mx-auto d-block img-fluid" alt="200x200" src="<?php echo $business_pan_mage; ?>" data-holder-rendered="true" style="width:200px; height:200px">
												</div>
											</div>
                                            <div class='row'> 
												<div class='col-md-4'>
                                                    Registration Certificate Image
													<img class="rounded mr-2 mx-auto d-block img-fluid" alt="200x200" src="<?php echo $registration_certificate_image; ?>" data-holder-rendered="true" style="width:200px; height:200px">
												</div>
												<div class='col-md-4'>
                                                    Web Logo
													<img class="rounded mr-2 mx-auto d-block img-fluid" alt="200x200" src="<?php echo $web_logo; ?>" data-holder-rendered="true" style="width:200px; height:200px">
												</div>
												<div class='col-md-4'>
                                                    App Logo
													<img class="rounded mr-2 mx-auto d-block img-fluid" alt="200x200" src="<?php echo $app_logo; ?>" data-holder-rendered="true" style="width:200px; height:200px">
												</div>
											</div>
                                            <div class='row'> 
												<div class='col-md-4'>
                                                    Web Restaurant Image
													<img class="rounded mr-2 mx-auto d-block img-fluid" alt="200x200" src="<?php echo $web_restaurant_image; ?>" data-holder-rendered="true" style="width:200px; height:200px">
												</div>
												<div class='col-md-4'>
                                                    App Restaurant Image
													<img class="rounded mr-2 mx-auto d-block img-fluid" alt="200x200" src="<?php echo $app_restaurant_image; ?>" data-holder-rendered="true" style="width:200px; height:200px">
												</div>
											</div>
                                        </div> 
                                    </div> 
								</div>	
							</div>
						</div>
						
						
						
						
					</div>
				</div>
			</div> <!-- end col -->
		</div> <!-- end row -->
								
	</div>
</div>
<?php
include_once'footer.php';
?>
                                       
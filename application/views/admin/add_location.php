<?php
include_once'header.php';
?>
<div class="page-content-wrapper ">
	<div class="container-fluid">
		<div class='row'>  
			<div class="col-md-12 col-xl-12">
				<div class="card m-b-30 m-t-30">
					<div class="card-body">
						<h4 class="mt-0 header-title">Add Location</h4>					
						<form action= "<?php echo base_url() ?>Location/addLocations" method="POST" enctype="multipart/form-data" class="mb-0">
							<div class="form-row">
								<div class="form-group col-md-6">
								<label for="inputCountry" class="bmd-label-floating">Country</label>
								    <select class="form-control mb-3 custom-select" name="country_id" id="countryId" required>
	                                     <option value="">select Country </option>
	                                     <?php 
	                                        foreach($resultCnt as $val)
	                                        {
	                                            echo '<option value="'.$val['id'].'">'.$val['country_name'].'</option>';
	                                        }
	                                     ?> 
                                    </select>   
								</div>
								<div class="form-group col-md-6">
								<label for="inputState" class="bmd-label-floating">State</label>
								    <select id="stateId" class="form-control mb-3 custom-select" name="state_id" required>  
								      <option>Select State</option>
                                   </select>  
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
								<label for="inputDistrict" class="bmd-label-floating">District</label>
								     <select id="districtId" class="form-control mb-3 custom-select" name="district_id" required>  
								      <option>Select District</option>
                                   </select>
								</div>
								<div class="form-group col-md-6">
								<label for="inputCity" class="bmd-label-floating">City</label>
								 <select id="cityId" class="form-control mb-3 custom-select" name="city_id" required>  
								      <option>Select City</option>
                                   </select>
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
								<label for="inputPincode" class="bmd-label-floating">Pincode</label>
								 <select id="pincodeId" class="form-control mb-3 custom-select" name="pincode_id" required>  
								      <option>Select Pincode</option>
                                   </select>
								</div>
								<div class="form-group col-md-6">
								<label for="inputLocation" class="bmd-label-floating">Location</label>
								<input type="text" class="form-control" name="location_name" required>
								</div>
							</div>
							<button type="submit" class="btn btn-raised btn-primary mb-0">Submit</button>
						</form>
					</div>
				</div>
			</div> <!-- end col -->
		</div> <!-- end row -->
								
	</div>
</div>
<?php
include_once'footer.php';
?>
                                       
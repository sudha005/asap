<?php
include_once'header.php';
?>
<div class="page-content-wrapper ">
	<div class="container-fluid">
		<div class='row'> 
			<div class="col-md-12 col-xl-12">
				<div class="card m-b-30 m-t-30">
					<div class="card-body">
						<h4 class="mt-0 header-title">Add Subcategory</h4>					
						<form class="mb-0" action="<?php echo base_url() ?>addSubcategory" 
                            method="POST" enctype="multipart/form-data">
							<div class="form-row">
								<div class="form-group col-md-6">
                                    <label for="masterCategory" class="bmd-label-floating">Master Category</label>
                                    <select id="masterCategory" class="form-control mb-3 custom-select" name="master_category_id" required>
                                        <option value="">Select Master Category</option>
                                        <?php                    
                                            $count = count(array_filter($resultCnt));
                                            if($count > 0) {
                                            $i=0;
                                            foreach($resultCnt as $key => $row){
                                            ?>
                                                <option value="<?php echo  $row['id'] ?>"><?php echo  $row['master_category_name'] ?></option>
                                            <?php
                                            }
                                            }
                                        ?>
                                    </select>
								</div>
                                <div class="form-group col-md-6">
                                    <label for="Category" class="bmd-label-floating"> Category</label>
                                    <select id="catId" class="form-control mb-3 custom-select" name="category_id" required>
                                        <option value="">Select  Category</option>
                                    </select>
								</div>
                            </div>
                            <div class="form-row">
								<div class="form-group col-md-6">
                                    <label for="SubCategory" class="bmd-label-floating">SubCategory</label>
                                    <input type="text" class="form-control" id="SubCategory" name="subcategory_name" required>
								</div>
							</div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="webImage" class="bmd-label-floating">Web Image</label>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div  style='display:none' class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                        <div>
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="subcat_webimage" required>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="appImage" class="bmd-label-floating">App Image</label>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div style='display:none' class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                        <div>
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="subcat_appimage" required>
                                            </span>
                                        </div>
                                    </div>
                                </div>
							</div>							
							<button type="submit" class="btn btn-raised btn-primary mb-0">Submit</button>
						</form>
					</div>
				</div>
			</div> <!-- end col -->
		</div> <!-- end row -->
								
	</div>
</div>
<?php
include_once'footer.php';
?>
                                       
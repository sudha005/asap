	</div> <!-- content -->

                <footer class="footer">
                    © 2018 Asap
                </footer>
            </div>
            <!-- End Right content here -->
        </div>
       <!-- jQuery  -->
        <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
		<script src="<?php echo base_url() ?>assets/js/tinymce.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/summernote-updated.min.js"></script>
		<script>
        		$(document).ready(function() {
            		  $('.summernote').summernote();
                      $("#countryId").change(function(){
                        id =$(this).val();
                        $.ajax({
                          url: '<?php echo base_url() ?>/state/state',
                          type: 'POST',
                          data: 'id='+id,
                          dataType: 'html',
                          success: function(response) {
                              
                            $("#stateId").html(response);
                          },
                          error: function(jqXHR, textStatus, errorThrown){
                             console.log(textStatus, errorThrown);
                         }
                       });
                    });
                  $("#stateId").change(function(){
                        id =$(this).val();
                        $.ajax({
                          url: '<?php echo base_url() ?>district/district',
                          type: 'POST',
                          data: 'id='+id,
                          dataType: 'html',
                          success: function(response) {
                              //alert(response);
                            $("#districtId").html(response);
                          },
                          error: function(jqXHR, textStatus, errorThrown){
                             console.log(textStatus, errorThrown);
                         }
                       });
                    });
                    $("#districtId").change(function(){
                        id =$(this).val();
                        $.ajax({
                          url: '<?php echo base_url() ?>city/city',
                          type: 'POST',
                          data: 'id='+id,
                          dataType: 'html',
                          success: function(response) {
                              //alert(response);
                            $("#cityId").html(response);
                          },
                          error: function(jqXHR, textStatus, errorThrown){
                             console.log(textStatus, errorThrown);
                         }
                       });
                    });
                $("#cityId").change(function(){
                        id =$(this).val();
                        $.ajax({
                          url: '<?php echo base_url() ?>pincode/pincode',
                          type: 'POST',
                          data: 'id='+id,
                          dataType: 'html',
                          success: function(response) {
                              //alert(response);
                            $("#pincodeId").html(response);
                          },
                          error: function(jqXHR, textStatus, errorThrown){
                             console.log(textStatus, errorThrown);
                         }
                       });
                        $.ajax({
                          url: '<?php echo base_url() ?>/City/cityCode',
                          type: 'POST',
                          data: 'id='+id,
                          dataType: 'html',
                          success: function(response) {
                              //alert(response);
                            $("#cityCode").val(response);
                          },
                          error: function(jqXHR, textStatus, errorThrown){
                            console.log(textStatus, errorThrown);
                         }
                       });
                    });
                $("#pincodeId").change(function(){
                    id =$(this).val();
                    $.ajax({
                      url: '<?php echo base_url() ?>Location/location',
                      type: 'POST',
                      data: 'id='+id,
                      dataType: 'html',
                      success: function(response) {
                          //alert(response);
                        $("#locationId").html(response);
                      },
                      error: function(jqXHR, textStatus, errorThrown){
                         console.log(textStatus, errorThrown);
                     }
                   });
                });
                $("#masterCategory").change(function(){
                    id =$(this).val();
                    $.ajax({
                      url: '<?php echo base_url() ?>/Category/catAjax',
                      type: 'POST',
                      data: 'id='+id,
                      dataType: 'html',
                      success: function(response) {
                         // alert(response);
                        $("#catId").html(response);
                      },
                      error: function(jqXHR, textStatus, errorThrown){
                        console.log(textStatus, errorThrown);
                     }
                   });
                   });
                $("#catId").change(function(){
                    id =$(this).val();
                    $.ajax({
                      url: '<?php echo base_url() ?>/Subcategory/subcatAjax',
                      type: 'POST',
                      data: 'id='+id,
                      dataType: 'html',
                      success: function(response) {
                         // alert(response);
                        $("#subCatId").html(response);
                      },
                      error: function(jqXHR, textStatus, errorThrown){
                        console.log(textStatus, errorThrown);
                     }
                   });
                   });
                setTimeout(function(){ $('.updateSuss').hide(); }, 3000);
    		});
		</script>
        <script src="<?php echo base_url() ?>assets/js/popper.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/bootstrap-material-design.js"></script>
        <script src="<?php echo base_url() ?>assets/js/modernizr.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/detect.js"></script>
        <script src="<?php echo base_url() ?>assets/js/fastclick.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.slimscroll.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.blockUI.js"></script>
        <script src="<?php echo base_url() ?>assets/js/waves.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.nicescroll.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.scrollTo.min.js"></script>

        <!-- Plugins js -->
        <script src="<?php echo base_url() ?>assets/plugins/timepicker/moment.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/timepicker/tempusdominus-bootstrap-4.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/timepicker/bootstrap-material-datetimepicker.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/clockpicker/jquery-clockpicker.min.js"></script>       
        <script src="<?php echo base_url() ?>assets/plugins/select2/select2.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js"></script>
   
        <!-- Plugins Init js -->
        <script src="<?php echo base_url() ?>assets/pages/form-advanced.js"></script>
		 <!--Wysiwig js-->
        
       
        <!-- App js -->
        <script src="<?php echo base_url() ?>assets/js/app.js"></script>
    </body>
</html>
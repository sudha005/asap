<?php
include_once'header.php';
?>
<div class="page-content-wrapper ">
	<div class="container-fluid">
		<div class='row'>  
			<div class="col-md-12 col-xl-12">
				<div class="card m-b-30 m-t-30">
					<div class="card-body">
						<h4 class="mt-0 header-title">View Category</h4>
						<?php echo $this->session->flashdata('msg'); ?>
						<div class="row">
							<div class="col-md-6 col-xl-6"></div>
							<div class="col-md-6 col-xl-6">
								<form action='<?php echo base_url() ?>Category/viewCategory' method='GET'>
								<div class="form-row pull-right">
									<div class="form-group col-md-8">
										<label for="inputEmail4">search</label>
										<input type="text" name='search' value="<?php echo $searchVal; ?>"  id="inputEmail4" class="form-control">
									</div>
									<div class="form-group col-md-3">
										<button type="submit" class="btn btn-raised btn-primary m-t-20">Search</button>
									</div>
								</div>
								</form>
							</div>
						</div>
						<table class="table mb-0">
							<thead class="thead-default">
								<tr>
									<th>S.No</th>
									<th>Category</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
                                <?php
                                  $i =$this->uri->segment(3)+1;
                                    $count = count(array_filter($result));
                                    if($count > 0) {
                                        foreach($result as $key => $row){
                                ?>
								<tr>
									<th scope="row"><?php echo $i;?></th>
									<td><?php echo $row['category_name'];?></td>
									<td><a href="<?php echo base_url(); ?>Category/editCategory/<?php echo $row['id'] ?>"><i class="mdi mdi-lead-pencil"></i></a>
									<?php
                                        if($row['isactive']==1){
                                        ?>
                                        <a onclick="return confirm('Confirm to Enable?');" href="<?php echo base_url()?>Category/catEnable/<?php echo $row['id'];?>"><i class="mdi mdi-close-circle"></i></a> 
                                        <?php
                                        }else{
                                        ?>
                                        <a onclick="return confirm('Confirm to Disable?');" href="<?php echo base_url()?>Category/catDisable/<?php echo $row['id'];?>" style="cursor:pointer"><i class="mdi mdi-check"></i></a>
                                        <?php
                                        }
                                    ?>
									<a data-toggle="modal" data-target="#myModal<?php echo $i; ?>"><i class="mdi mdi-eye"></i></a>
									<div class="modal fade"
									id="myModal<?php echo $i; ?>" 
									tabindex="-1" 
									role="dialog" 
									aria-labelledby="myModalLabel<?php echo $i; ?>">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title">Category Detail</h4>
											</div>
											<div class="modal-body">
												<table>
												<tr>
												<th>Master Category:</th>
												<td><?php echo $row['mastercategory']; ?></td>
												</tr>
												<tr>
												<th>Category:</th>
												<td><?php echo $row['category_name']; ?></td>
												</tr>
												<tr>
												<th>Category Web Image:</th>
												<td><img src='<?php echo base_url() ?>uploads/category_webimage/<?php echo $row['category_webimage']; ?>' width="150" height="150"></td>
												</tr>
												<tr>
												<th>Category App Image:</th>
												<td><img src='<?php echo base_url() ?>uploads/category_appimage/<?php echo $row['category_appimage']; ?>' width="150" height="150"></td>
												</tr>
												</table>
											</div>
										</div><!-- /.modal-content -->
										</div><!-- /.modal-content -->
									</div><!-- /.modal-content -->
                                    <a onclick="return confirm('Are You Sure to Delete ?');" href="<?php echo base_url()?>Category/deleteCategory/<?php echo $row['id'];?>" style="cursor:pointer"><i class="mdi mdi-delete"></i></a></td>
								</tr>
                                <?php
                                        $i++; }
                                    }else{
                                        ?>
                                    <tr><td colspan="7">No Category Found</td></tr>
                                        <?php
                                    }
                                    ?>
							</tbody>
						</table>
						<div class='col-md-12'>
							<nav aria-label="Page navigation" class='pull-right'>
								<?php echo $links; ?>
							</nav> 
						</div>
					</div>
				</div>
			</div> <!-- end col -->
		</div> <!-- end row -->
								
	</div>
</div>
<?php
include_once'footer.php';
?>
                                       
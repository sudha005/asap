<?php
include_once'header.php';
?>
<div class="page-content-wrapper ">
	<div class="container-fluid">
		<div class='row'>  
			<div class="col-md-12 col-xl-12">
				<div class="card m-b-30 m-t-30">
					<div class="card-body">
						<h4 class="mt-0 header-title">View Pincode</h4>
						<?php echo $this->session->flashdata('msg'); ?>
						<div class="row">
							<div class="col-md-6 col-xl-6"></div>
							<div class="col-md-6 col-xl-6">
								<form action='<?php echo base_url() ?>Pincode/viewpincode' method='GET'>
								<div class="form-row pull-right">
									<div class="form-group col-md-8">
										<label for="inputEmail4">search</label>
										<input type="text" name='search' value="<?php echo $searchVal; ?>"  id="inputEmail4" class="form-control">
									</div>
									<div class="form-group col-md-3">
										<button type="submit" class="btn btn-raised btn-primary m-t-20">Search</button>
									</div>
								</div>
								</form>
							</div>
						</div>
						<table class="table mb-0">
							<thead class="thead-default">
								<tr>
									<th>Sno</th>
	                                <th>Country</th>
	                                <th>State Name</th>
	                                <th>District Name</th>
	                                <th>City Name</th>
	                                <th>Pincode</th>
	                                <th>Action</th>
								</tr>
							</thead>
							<tbody>
                                   <?php
                                   $i =$this->uri->segment(3)+1;
                                    $count = count(array_filter($result));
                                    if($count > 0) {
                                        foreach($result as $key => $row){
                                    ?>
                                   <tr class="zebra-striping">
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $row['country'];?></td>
                                        <td><?php echo $row['state'];?></td>
                                        <td><?php echo $row['district'];?></td>
                                        <td><?php echo $row['city'];?></td>
                                        <td><?php echo $row['pincode'];?></td>
                                        <td>
                                            <a href="<?php echo base_url(); ?>pincode/editpincode/<?php echo $row['id'] ?>"><i class="mdi mdi-lead-pencil"></i></a>
                                            <?php
                                            if($row['isactive']==1){
                                            ?>
                                            <a onclick="return confirm('Confirm to Enable?');" href="<?php echo base_url()?>pincode/pincodeEnable/<?php echo $row['id'];?>"> <i class="mdi mdi-close-circle"></i></a> 
                                           <?php
                                            }else{
                                           ?>
                                            <a onclick="return confirm('Confirm to Disable?');" href="<?php echo base_url()?>pincode/pincodeDisable/<?php echo $row['id'];?>" style="cursor:pointer"><i class="mdi mdi-check"></i></a>
                                            <?php
                                            }
                                            ?>
                                            <a onclick="return confirm('Are You Sure to Delete ?');" href="<?php echo base_url()?>pincode/deletepincode/<?php echo $row['id'];?>" style="cursor:pointer"><i class="mdi mdi-delete"></i></a>  
                                        </td>
                                    </tr>
                                    <?php
                                       $i++; }
                                    }else{
                                        ?>
                                    <tr><td colspan="7">No Pincode Found</td></tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
						</table> 
						<div class='col-md-12'>
                                <div class='pull-right'><?php echo $links; ?></div>
                            </div>
						<!-- <div class='col-md-12'>
							<nav aria-label="Page navigation pull-right">
								<ul class="pagination justify-content-end">
									<li class="page-item disabled">
										<a class="page-link" href="#" tabindex="-1">Previous</a>
									</li>
									<li class="page-item"><a class="page-link" href="#">1</a></li>
									<li class="page-item"><a class="page-link" href="#">2</a></li>
									<li class="page-item"><a class="page-link" href="#">3</a></li>
									<li class="page-item">
										<a class="page-link" href="#">Next</a>
									</li>
								</ul>
							</nav> 
						</div> -->
					</div>
				</div>
			</div> <!-- end col -->
		</div> <!-- end row -->
								
	</div>
</div>
<?php
include_once'footer.php';
?>
                                       
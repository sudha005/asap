<?php
include_once'header.php';
?>
<div class="page-content-wrapper ">
	<div class="container-fluid">
		<div class='row'>  
			<div class="col-md-12 col-xl-12">
				<div class="card m-b-30 m-t-30">
					<div class="card-body">
						<h4 class="mt-0 header-title">Basic Settings</h4>					
						<form enctype="multipart/form-data" method="POST" action="<?php echo base_url() ?>/Basicsettings/updatebasic_settings" class="mb-0">
							<?php echo $this->session->flashdata('msg'); ?>
							<div class="form-row">
								<div class="form-group col-md-6">
								<label for="inputSitetitle" class="bmd-label-floating">Site Title</label>
								<input type="text" class="form-control" name="site_title" value="<?php echo $result['site_title']; ?>" required>
								</div>
								<div class="form-group col-md-6">
								<label for="inputAdmintitle" class="bmd-label-floating">Admin Title</label>
								<input type="text" class="form-control" name="admin_title" value="<?php echo $result['admin_title']; ?>" required>
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
								<label for="inputEmail" class="bmd-label-floating">Support Email</label>
								<input type="email" class="form-control" name="support_email" value="<?php echo $result['support_email']; ?>" required>
								</div>
								<div class="form-group col-md-6">
								<label for="inputMobile" class="bmd-label-floating">Support Mobile Number</label>
								<input type="text" class="form-control" name="contact_phone" pattern="[0-9]{10}" value="<?php echo $result['contact_phone']; ?>" required >
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
								<label for="inputEnqueryemail" class="bmd-label-floating">Enquiry Email</label>
								<input type="email" class="form-control" name="enquiry_email" value="<?php echo $result['enquiry_email']; ?>" required>
								</div>
								<div class="form-group col-md-6">
								<label for="inputOrderemail" class="bmd-label-floating">Order Email</label>
								<input type="email" class="form-control" name="order_email" value="<?php echo $result['order_email']; ?>" required>
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
								<label for="inputContactemail" class="bmd-label-floating">Contact Email</label>
								<input type="email" class="form-control" name="contact_email" value="<?php echo $result['contact_email']; ?>" required>
								</div>
								<div class="form-group col-md-6">
                                    <label for="webLogo" class="bmd-label-floating">Web Logo</label>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                        	<img src="<?php echo $Weblogo;?>" width="150 px" height="150 px">
                                        </div>
                                        <div>
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="web_logo">
                                            </span>
                                        </div>
                                    </div>
                                </div>
							</div>
							<div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="appLogo" class="bmd-label-floating">App Logo</label>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                        	<img src="<?php echo $Applogo; ?>" width="150 px" height="150 px">
                                        </div>
                                        <div>
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="app_logo">
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="appFavicon" class="bmd-label-floating">Fav Icon</label>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                        	<img src="<?php echo $Favicon; ?>" width="150 px" height="150 px">
                                        </div>
                                        <div>
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="fav_icon">
                                            </span>
                                        </div>
                                    </div>
                                </div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
								<label for="inputSmsphone" class="bmd-label-floating">SMS Phone Number</label>
								<input type="text" class="form-control" name="sms_phone_number" pattern="[0-9]{10}" value="<?php echo $result['sms_phone_number']; ?>" required>
								</div>
								<div class="form-group col-md-6">
								<label for="inputFootertext" class="bmd-label-floating">Footer Text</label>
								<textarea id="textarea" class="form-control" name="footer_text" maxlength="225" rows="3"><?php echo $result['footer_text']; ?></textarea>
								</div>
							</div>

							<div class="form-row">
								<div class="form-group col-md-6">
								<label for="inputAddress" class="bmd-label-floating">Address </label>
								<textarea class="form-control" name="address" maxlength="225" rows="3"><?php echo $result['address']; ?></textarea>
								</div>
								<div class="form-group col-md-6">
								<label for="inputDistance" class="bmd-label-floating">Max Distance</label>
								<input type="number" class="form-control" name="max_distance" value="<?php echo $result['max_distance']; ?>" required>
								</div>
							</div>
							<input type="hidden" value="<?php echo $result['id']; ?>" name="id">				
							<button type="submit" class="btn btn-raised btn-primary mb-0">Submit</button>
						</form>
					</div>
				</div>
			</div> <!-- end col -->
		</div> <!-- end row -->
								
	</div>
</div>
<?php
include_once'footer.php';
?>
                                       